﻿using AutoMapper;
using ESDP.BL.AutoMapper.AcademicSubjectMapper;
using ESDP.BL.AutoMapper.AcademicYearMapper;
using ESDP.BL.AutoMapper.IdentifierClassMapper;
using ESDP.BL.AutoMapper.ReportPeriodMapper;
using ESDP.BL.AutoMapper.TeacherMapper;
using ESDP.BL.AutoMapper.UserMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper
{
    public class AutoMapperConfig
    {
        public MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainToUserDtoMappingProfile());
                cfg.AddProfile(new UserDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToAcademicSubjectDtoMappingProfile());
                cfg.AddProfile(new AcademicSubjectDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToIdentifierClassMappingProfile());
                cfg.AddProfile(new CreateIdentifierClassDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToReportPeriodDtoMappingProrile());
                cfg.AddProfile(new ReportPeriodDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToAcademicYearDtoMappingProfile());
                cfg.AddProfile(new AcademicYearDtoToDomainMappingProfile());
                cfg.AddProfile(new DomainToTeacherDtoMappingProfile());
                cfg.AddProfile(new TeacherDtoToDomainMappingProfile());
            });
        }
    }
}
