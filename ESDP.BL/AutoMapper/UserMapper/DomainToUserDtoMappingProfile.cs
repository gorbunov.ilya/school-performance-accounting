﻿using AutoMapper;
using ESDP.Common.Dtos.UserDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.UserMapper
{
    public class DomainToUserDtoMappingProfile : Profile
    {
        public DomainToUserDtoMappingProfile()
        {
            CreateUserMap();
            CreateUserEditMap();
        }

        public void CreateUserMap()
        {
            CreateMap<User, UserManagerDto>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.Email, opts => opts.MapFrom(vm => vm.Email))
                .ForMember(src => src.FullName, opts => opts.MapFrom(vm => vm.Name + " " + vm.Surname + " " + vm.MiddleName))
                .ForMember(src => src.IsBlocked, opts => opts.MapFrom(vm => vm.IsBlocked.ToString()))
                .ForMember(src => src.IsDeleted, opts => opts.MapFrom(vm => vm.IsDeleted.ToString()));
        }
        public void CreateUserEditMap()
        {
            CreateMap<User, UserEditDto>()
                .ForMember(src => src.Name, opts => opts.MapFrom(vm => vm.Name))
                .ForMember(src => src.Surname, opts => opts.MapFrom(vm => vm.Surname))
                .ForMember(src => src.MiddleName, opts => opts.MapFrom(vm => vm.MiddleName))
                .ForMember(src => src.Email, opts => opts.MapFrom(vm => vm.Email))
                .ForMember(src => src.Password, opts => opts.MapFrom(vm => vm.PasswordHash))
                .ForMember(src => src.isBlocked, opts => opts.MapFrom(vm => vm.IsBlocked))
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id));
        }
    }
}
