﻿using AutoMapper;
using ESDP.Common.Dtos.IdentifierClassDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.IdentifierClassMapper
{
    public class DomainToIdentifierClassMappingProfile : Profile
    {
        public DomainToIdentifierClassMappingProfile()
        {
            CreateIdentifierClassMap();
        }

        public void CreateIdentifierClassMap()
        {
            CreateMap<IdentifierClass, IdentifierClassDto>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.Identifier, opts => opts.MapFrom(vm => vm.Parallel.ToString() + vm.Letter));
        }
    }
}
