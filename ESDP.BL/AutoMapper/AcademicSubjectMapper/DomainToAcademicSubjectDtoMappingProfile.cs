﻿using AutoMapper;
using ESDP.Common.Dtos.AcademicSubjectDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.AcademicSubjectMapper
{
    public class DomainToAcademicSubjectDtoMappingProfile : Profile
    {
        public DomainToAcademicSubjectDtoMappingProfile()
        {
            CreateAcademicSubjectMap();
        }

        public void CreateAcademicSubjectMap()
        {
            CreateMap<AcademicSubject, AcademicSubjectDto>()
                .ForMember(src => src.Id, opts => opts.MapFrom(vm => vm.Id))
                .ForMember(src => src.Name, opts => opts.MapFrom(vm => vm.Name));
        }
    }
}
