﻿using AutoMapper;
using ESDP.Common.Dtos.AcademicYearDtos;
using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.BL.AutoMapper.AcademicYearMapper
{
    public class DomainToAcademicYearDtoMappingProfile : Profile
    {
        public DomainToAcademicYearDtoMappingProfile()
        {
            CreateAcademicYearMap();
        }

        public void CreateAcademicYearMap()
        {
            CreateMap<AcademicYear, AcademicYearDto>();
        }
    }
}
