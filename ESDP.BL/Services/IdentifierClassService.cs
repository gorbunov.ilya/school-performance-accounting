﻿using AutoMapper;
using ESDP.Common;
using ESDP.Common.Dtos.IdentifierClassDtos;
using ESDP.Data.Entities;
using ESDP.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.BL.Services
{
    public class IdentifierClassService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;

        public IdentifierClassService(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
        }

        public async Task<OperationResult> CreateAsync(CreateIdentifierClassDto createIdentifierClassDto)
        {
            var identifierClass = _mapper.Map<CreateIdentifierClassDto, IdentifierClass>(createIdentifierClassDto);
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var checkIdentifierClasses = await unitOfWork.IdentifierClassRepository.GetAllAsync();
                if(checkIdentifierClasses.FirstOrDefault(x => x.Letter == identifierClass.Letter && x.Parallel == identifierClass.Parallel) == null)
                {
                    await unitOfWork.IdentifierClassRepository.CreateAsync(identifierClass);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Идентификатор класса создан");
                }
                return new OperationResult(false, "Идентификатор с такими данными уже существует");
            }
        }

        public async Task<OperationResult> EditAsync(CreateIdentifierClassDto editIdentifierClassDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var identifierClass = await unitOfWork.IdentifierClassRepository.GetByIdAsync(editIdentifierClassDto.Id);
                var checkIdentifierClasses = await unitOfWork.IdentifierClassRepository.GetAllAsync();
                if (identifierClass != null && checkIdentifierClasses.FirstOrDefault(x => x.Letter == editIdentifierClassDto.Letter && x.Parallel == editIdentifierClassDto.Parallel) == null)
                {
                    identifierClass = _mapper.Map(editIdentifierClassDto, identifierClass);
                    unitOfWork.IdentifierClassRepository.UpdateAsync(identifierClass);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Идентификатор класса изменен");
                }
                return new OperationResult(false, "Идентификатор с такими данными уже существует");
            }
        }

        public async Task<OperationResult> DeleteAsync(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var identifierClass = await unitOfWork.IdentifierClassRepository.GetByIdAsync(id);
                if (identifierClass.AcademicClassId == null)
                {
                    identifierClass.IsDeleted = true;
                    unitOfWork.IdentifierClassRepository.UpdateAsync(identifierClass);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Идентификатор класса удален");
                }
                return new OperationResult(false, "Удаление невозможно. Данный идентификатор привязан к классу.");
            }
        }

        public async Task<List<IdentifierClassDto>> GetAllIdentifierClass(IdentifierClassFilterDto filterDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var identifierClasses = await unitOfWork.IdentifierClassRepository.GetAllAsync();
                var identifierClassViewModels = identifierClasses.Select(_mapper.Map<IdentifierClassDto>).ToList();
                if (filterDto.Identifier != null)
                {
                    return identifierClassViewModels.Where(x => x.Identifier.ToLower().Contains(filterDto.Identifier.ToLower()) && x.IsDeleted == false).ToList();
                }
                return identifierClassViewModels.Where(x => x.IsDeleted == false).ToList();
            }
        }

        public async Task<IdentifierClassDto> FindByIdAsync(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var identifierClass = await unitOfWork.IdentifierClassRepository.GetByIdAsync(id);
                var identifierDto = _mapper.Map<IdentifierClassDto>(identifierClass);
                return identifierDto;
            }
        }
    }
}
