﻿using AutoMapper;
using ESDP.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ESDP.Common.Dtos.UserDtos;

namespace ESDP.BL
{
    public class UserService
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public UserService(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration, IMapper mapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<User> CreateAsync(RegisterDto registerUser, string password)
        {
            var user = _mapper.Map<RegisterDto, User>(registerUser);
            await _userManager.CreateAsync(user, password);
            return user;
        }
        public async Task<UserEditDto> FindByIdEditModelAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var roles = GetAllRoles();
            var userRoles = await _userManager.GetRolesAsync(user);
            var editDto = _mapper.Map<User, UserEditDto>(user);
            editDto.UserRoles = userRoles;
            editDto.Roles = roles;
            return editDto;
        }

        public async Task AddToRoleAsync(User user, string role)
        {
            await _userManager.AddToRoleAsync(user, role);
        }

        public async Task<IList<string>> GetRolesAsync(User user)
        {
            return await _userManager.GetRolesAsync(user);
            
        }
        public async Task<User> FindByEmailAsync(string email)
        {
             return await _userManager.FindByEmailAsync(email);
        }

        public async Task<bool> CheckPasswordAsync(User user, string password)
        {
            return await _userManager.CheckPasswordAsync(user, password);
        }

        public async Task<List<UserManagerDto>> GetAllUsers(UserFilterDto filter)
        {
            var users = _userManager.Users.ToList();
            var userViewModels = users.Select(_mapper.Map<UserManagerDto>).ToList();
            for(int i = 0; i < users.Count; i++)
            {
                userViewModels[i].RolesName = await _userManager.GetRolesAsync(users[i]);
            }
            if(filter.Fullname != null && filter.SelectedRole == null)
            {
                return userViewModels.Where(u => u.FullName.ToLower().Contains(filter.Fullname.ToLower()) && u.IsDeleted == false).ToList();
            }
            if(filter.SelectedRole != null && filter.Fullname == null)
            {
                return userViewModels.Where(c => c.RolesName.Contains(filter.SelectedRole) && c.IsDeleted == false).ToList();
            }
            if(filter.SelectedRole != null && filter.Fullname != null)
            {
                return userViewModels.Where(u => u.FullName.ToLower().Contains(filter.Fullname.ToLower()) && u.RolesName.Contains(filter.SelectedRole) && u.IsDeleted == false).ToList();
            }
            return userViewModels.Where(c => c.IsDeleted == false).ToList();
        }

        public List<string> GetAllRoles()
        {
            return  _roleManager.Roles.Select(x => x.Name).ToList();
        }

        

        public async Task<JwtSecurityToken> GetToken(User user)
        {
            var claims = await GetValidClaims(user);

            var signinKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_configuration["Jwt:SigningKey"]));

            int expiryInMinutes = Convert.ToInt32(_configuration["Jwt:ExpiryInMinutes"]);

            var token = new JwtSecurityToken(
                issuer: _configuration["Jwt:Site"],
                audience: _configuration["Jwt:Site"],
                expires: DateTime.UtcNow.AddMinutes(expiryInMinutes),
                signingCredentials: new SigningCredentials(signinKey, SecurityAlgorithms.HmacSha256),
                claims: claims
            );
            return token;

        }

        public async Task<List<Claim>> GetValidClaims(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName)
            };
            var userRoles = await _userManager.GetRolesAsync(user);
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
            }
            return claims;
        }

        public async Task<bool> EditUser(UserEditDto editModel)
        {
            var user = await _userManager.FindByIdAsync(editModel.Id);
            if (user != null)
            {
                var roles = await _userManager.GetRolesAsync(user);
                await _userManager.RemoveFromRolesAsync(user, roles);
                await _userManager.AddToRolesAsync(user, editModel.Roles);
                user = _mapper.Map(editModel, user);
               var result = await _userManager.UpdateAsync(user);
                if(result.Succeeded)
                {
                    return true;
                }
                return false;
            }

            return false;
        }

        public async Task<bool> DeleteUserAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                user.IsDeleted = true;
                user.IsBlocked = true;
                await _userManager.UpdateAsync(user);

                return true;
            }

            return false;
        }

        public async Task<bool> BlockUnblockUserAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                if (user.IsBlocked)
                {
                    user.IsBlocked = false;
                    await _userManager.UpdateAsync(user);
                }
                else
                {
                    user.IsBlocked = true;
                    await _userManager.UpdateAsync(user);
                }

                return true;
            }

            return false;
        }
    }
}
