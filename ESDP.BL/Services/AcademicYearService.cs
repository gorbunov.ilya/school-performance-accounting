﻿using AutoMapper;
using ESDP.Common;
using ESDP.Common.Dtos.AcademicYearDtos;
using ESDP.Data.Entities;
using ESDP.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.BL.Services
{
    public class AcademicYearService
    {
        private readonly IUnitOfWorkFactory _unitOfOWorkFactory;
        private readonly IMapper _mapper;

        public AcademicYearService(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
        {
            _unitOfOWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
        }

        public async Task<OperationResult> CreateAsync(AcademicYearDto academicYearDto)
        {
            var academicYear = _mapper.Map<AcademicYearDto, AcademicYear>(academicYearDto);
            using(var unitOfWork = _unitOfOWorkFactory.MakeUnitOfWork())
            {
                var checkAcademicYears =  await unitOfWork.AcademicYearRepository.GetAllAsync();
                if(checkAcademicYears.FirstOrDefault(x => x.Name == academicYear.Name) == null)
                {
                    if(academicYear.BeginDate < academicYear.EndDate)
                    {
                        if (academicYear.IsCurrent == true)
                        {
                            CheckIsCurrent(checkAcademicYears, unitOfWork);
                        }
                        await unitOfWork.AcademicYearRepository.CreateAsync(academicYear);
                        await unitOfWork.CompleteAsync();
                        return new OperationResult(true, "Учебный год создан");
                    }
                    return new OperationResult(false, "Дата начала не может быть позже даты конца");
                }
                return new OperationResult(false, "Учебный год с таким именем уже существует");
            }
        }

        public async Task<OperationResult> EditAsync(AcademicYearDto academicYearDto)
        {
            using (var unitOfWork = _unitOfOWorkFactory.MakeUnitOfWork())
            {
                var academicYear = await unitOfWork.AcademicYearRepository.GetByIdAsync(academicYearDto.Id);
                var checkAcademicYears = await unitOfWork.AcademicYearRepository.GetAllAsync();
                checkAcademicYears = checkAcademicYears.Where(x => x.Id != academicYearDto.Id).ToList();
                if(checkAcademicYears
                        .FirstOrDefault(x => x.Name == academicYearDto.Name) == null 
                   || academicYear.IsCurrent != academicYearDto.IsCurrent 
                   || academicYear.BeginDate != academicYearDto.BeginDate 
                   || academicYear.EndDate != academicYearDto.EndDate)
                {
                    if (academicYearDto.BeginDate < academicYear.EndDate)
                    {
                        academicYear = _mapper.Map(academicYearDto, academicYear);
                        if (academicYear.IsCurrent == true)
                        {
                            CheckIsCurrent(checkAcademicYears, unitOfWork);
                        }
                        unitOfWork.AcademicYearRepository.UpdateAsync(academicYear);
                        await unitOfWork.CompleteAsync();
                        return new OperationResult(true, "Учебный год отредактирован");
                    }
                    return new OperationResult(false, "Дата начала не может быть позже даты конца");
                }
                return new OperationResult(false, "Учебный год с таким именем уже существует");
            }
        }

        public async Task<AcademicYear> GetByIdAsync(int id)
        {
            using (var unitOfWork = _unitOfOWorkFactory.MakeUnitOfWork())
            {
                return await unitOfWork.AcademicYearRepository.GetByIdAsync(id);
            }
        }

        public async Task<OperationResult> DeleteAsync(int id)
        {
            using(var unitOfWork = _unitOfOWorkFactory.MakeUnitOfWork())
            {
                var academicYear = await unitOfWork.AcademicYearRepository.GetByIdIncludingAsync(id);
                if(academicYear.Periods.Count == 0)
                {
                    academicYear.IsDeleted = true;
                    unitOfWork.AcademicYearRepository.UpdateAsync(academicYear);
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(true, "Учебный год удален");
                }
                return new OperationResult(false, "Удаление невозможно. У данного учебного год имеются отчетные периоды.");
            }
        }

        public async Task<List<AcademicYearDto>> GetAllAcademicYears(AcademicYearFilterDto filterDto)
        {
            using(var unitOfWork = _unitOfOWorkFactory.MakeUnitOfWork())
            {
                var academicYears = await unitOfWork.AcademicYearRepository.GetAllAsync();
                var academicYearsViewModels = academicYears.Select(_mapper.Map<AcademicYearDto>).ToList();
                if(filterDto.Name != null)
                {
                    return academicYearsViewModels.Where(x => x.Name.ToLower().Contains(filterDto.Name.ToLower()) && x.IsDeleted == false).ToList();
                }
                return academicYearsViewModels.Where(x => x.IsDeleted == false).ToList();
            }
        }

        private void CheckIsCurrent(List<AcademicYear> academicYears, IUnitOfWork unitOfWork)
        {
            var currentAcademicYear = academicYears.FirstOrDefault(x => x.IsCurrent == true);
            if(currentAcademicYear != null)
            {
                currentAcademicYear.IsCurrent = false;
                unitOfWork.AcademicYearRepository.UpdateAsync(currentAcademicYear);
            }
        }
    }
}
