﻿using AutoMapper;
using ESDP.Common;
using ESDP.Common.Dtos.AcademicSubjectDtos;
using ESDP.Common.Dtos.TeachersDtos;
using ESDP.Common.Dtos.UserDtos;
using ESDP.Data.Entities;
using ESDP.Data.UnitOfWork;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.BL.Services
{
    public class TeachersService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;
        private readonly AcademicSubjectService _subjectService;
        private readonly UserService _userService;

        public TeachersService(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper, AcademicSubjectService subjectService, UserService userService)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
            _subjectService = subjectService;
            _userService = userService;
        }

        public async Task<OperationResult> CreateAsync(TeacherDto teacherDto)
        {
            TeachersAcademicSubjects[] teachersAcademicSubjects = new TeachersAcademicSubjects[teacherDto.AcademicSubjectsId.Count];
            for(int i = 0; i < teacherDto.AcademicSubjectsId.Count; i++)
            {
                teachersAcademicSubjects[i] = _mapper.Map<TeacherDto, TeachersAcademicSubjects>(teacherDto);
                teachersAcademicSubjects[i].AcademicSubjectsId = teacherDto.AcademicSubjectsId[i];
            }
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                await unitOfWork.TeachersAcademicSubjectsRepository.CreateRangeAsync(teachersAcademicSubjects);
                await unitOfWork.CompleteAsync();
                return new OperationResult(true, "Выбранные предметы назначены учителю");
            }
        }

        public async Task<OperationResult> EditAsync(TeacherDto teacherDto)
        {
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var teachersAcademicSubjects = await unitOfWork.TeachersAcademicSubjectsRepository.GetAllAsync();
                unitOfWork.TeachersAcademicSubjectsRepository.RemoveRange(teachersAcademicSubjects.Where(x => x.UserId == teacherDto.UserId).ToArray());
                TeachersAcademicSubjects[] teachers = new TeachersAcademicSubjects[teacherDto.AcademicSubjectsId.Count];
                for(int i = 0; i < teacherDto.AcademicSubjectsId.Count; i++)
                {
                    teachers[i] = _mapper.Map<TeacherDto, TeachersAcademicSubjects>(teacherDto);
                    teachers[i].AcademicSubjectsId = teacherDto.AcademicSubjectsId[i];
                }
                await unitOfWork.TeachersAcademicSubjectsRepository.CreateRangeAsync(teachers);
                await unitOfWork.CompleteAsync();
                return new OperationResult(true, "Предметы изменены");
            }
        }
        
        public async Task<OperationResult> DeleteAsync(string id)
        {
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var teacher = await unitOfWork.TeachersAcademicSubjectsRepository.GetAllIncludingAsync();
                teacher = teacher.Where(x => x.UserId == id);
                if(teacher.FirstOrDefault(x => x.UserId == id).Subjects.Periods.Count == 0)
                {
                    foreach(var item in teacher)
                    {
                        item.IsDeleted = true;
                        
                    }
                    unitOfWork.TeachersAcademicSubjectsRepository.UpdateRange(teacher.ToArray());
                    await unitOfWork.CompleteAsync();
                    return new OperationResult(false, "Данный учитель удален");
                }
                return new OperationResult(false, "Удаление невозможно. Предметы данного учителя привязаны к отчетному периоду.");
            }
        }

        public async Task<List<TeacherManagerDto>> GetAllTeacher(TeacherFilterDto filterDto)
        {
            using(var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var teachers = await unitOfWork.TeachersAcademicSubjectsRepository.GetAllIncludingAsync();
                var teachersViewModel = teachers.Select(_mapper.Map<TeacherManagerDto>).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();
                for(int i = 0; i < teachersViewModel.Count; i++)
                {
                    teachersViewModel[i].SubjectsName = new List<string>();
                    for(int j = 0; j < teachers.Where(x => x.UserId == teachersViewModel[i].Id).ToList().Count; j++)
                    {
                        teachersViewModel[i].SubjectsName.Add(teachers.Where(x => x.UserId == teachersViewModel[i].Id).ToList()[j].Subjects.Name);
                    }
                }
                if(filterDto.Fullname != null && filterDto.SelectedSubject == null)
                {
                    return teachersViewModel.Where(x => x.Fullname.ToLower().Contains(filterDto.Fullname.ToLower()) && x.IsDeleted == false).ToList();
                }
                if(filterDto.SelectedSubject != null && filterDto.Fullname == null)
                {
                    return teachersViewModel.Where(x => x.SubjectsName.Contains(filterDto.SelectedSubject) && x.IsDeleted == false).ToList();
                }
                if(filterDto.Fullname != null && filterDto.SelectedSubject != null)
                {
                    return teachersViewModel.Where(x => x.Fullname.ToLower().Contains(filterDto.Fullname.ToLower()) && x.SubjectsName.Contains(filterDto.SelectedSubject) && x.IsDeleted == false).ToList();               
                }
                return teachersViewModel.Where(x => x.IsDeleted == false).ToList();
            }
        }

        public async Task<List<AcademicSubjectDto>> GetAllSubject()
        {
            return await _subjectService.GetAllAcademicSubject();
        }

        public async Task<List<UserManagerDto>> GetTeacher()
        {
            UserFilterDto filterDto = new UserFilterDto
            {
                SelectedRole = "Teacher"
            };
            return await _userService.GetAllUsers(filterDto);
        }

    }
}
