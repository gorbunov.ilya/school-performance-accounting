import React, { PureComponent } from 'react';
import { Route, Switch, HashRouter } from 'react-router-dom';
import '../styles/bootstrap.css';
import SignIn from '../Pages/Authorization/SignIn';
import Home from '../Pages/Home/Home';
import UserManager from '../Pages/AdminPages/UserManager';
import PageCreateUser from '../Pages/UserPages/PageCreateUser';
import PageEditUser from '../Pages/UserPages/PageEditUser';
import AdminPage from '../Pages/AdminPages/AdminPage';
import AuthService from '../services/AuthService';
import IdentifierClassManager from '../Pages/IdentifierClass/IdentifierClassManager';
import PageCreateIdentifierClass from '../Pages/IdentifierClass/PageCreateIdentifierClass';
import PageEditIdentifierClass from '../Pages/IdentifierClass/PageEditIdentifierClass';
import AcademicSubjects from '../Pages/AcademicSubject/AcademicSubjectsPage';
import AcademicSubjectCreatePage from '../Pages/AcademicSubject/AcademicSubjectCreatePage';
import AcademicSubjectEditPage from '../Pages/AcademicSubject/AcademicSubjectEditPage';
import AcademicYears from '../Pages/AcademicYear/AcademicYearsPage';
import AcademicYearCreatePage from '../Pages/AcademicYear/AcademicYearCreatePage';
import AcademicYearEditPage from '../Pages/AcademicYear/AcademicYearEditPage';

class App extends PureComponent {
    constructor(props) {
        super(props)
        this.authService = new AuthService();
    }
    render() {
        let AdminRoute;
        let TeacherRoute;
        let AllRoute = [
            <Route path="/" exact component={SignIn} key={'signIn'} />,
            <Route path="/home" exact component={Home} key={'home'} />,            
            ];
            
                AdminRoute = [
                    //Subjects Pages
            <Route path="/subjectsmanager/"  component={AcademicSubjects} key={'academicSubject'}/>,
            <Route path="/add-subject" exact component={AcademicSubjectCreatePage} key={'academicSubjectCreate'} />,
            <Route path="/edit-subject/"  component={AcademicSubjectEditPage} key={'academicSubjectEdit'} />,
                    //AdminPages
            <Route path="/admin" exact component={AdminPage} key={'adminPage'}/>,
            <Route path="/usermanager" component={UserManager} key={'userManager'}/>,
            <Route path="/add" exact component={PageCreateUser} key={'pageCreateUser'}/>,
            <Route path="/edit/" component={PageEditUser} key ={'pageEditUser'}/>,
                    //Years Pages
            <Route path="/yearsmanager/" component={AcademicYears} key ={'academicYears'}/>,
            <Route path="/add-year" exact component={AcademicYearCreatePage} key ={'academicYearsCreate'}/>,
            <Route path="/edit-year/"  component={AcademicYearEditPage} key ={'academicYearsEdit'}/>,
                    //IdentifierClass Pages
            <Route path="/identifierclassmanager/" component={IdentifierClassManager} key={'identifierClassManager'}/>,
            <Route path="/addidentifierclass" exact component={PageCreateIdentifierClass} key={'pageCreateIdentifierClass'}/>,
            <Route path="/editidentifierclass" component={PageEditIdentifierClass} key ={'pageEditIdentifierClass'}/>];
                

        return (
            <HashRouter>
                <Switch>
                    {AdminRoute}
                    {TeacherRoute}
                    {AllRoute}
                </Switch>
            </HashRouter>
        )
    }

}




export default App;

