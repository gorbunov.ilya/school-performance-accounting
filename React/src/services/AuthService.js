
import axios from './axios'
export default class AuthService {
    isAuthenticated() {
        const token = localStorage.getItem('token')
        return token !== null ? true : false
    }
    login(token, roles) {
        localStorage.token = token;
        localStorage.roles = roles;
    }
    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('roles');

    }
    passwordValidator = (password) => {
        return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/.test(password);
    }
    checkForRoles() {
        const roles = localStorage.getItem('roles')
        return roles;
    }
    setAuthToken = (token) => {
        axios.defaults.headers.Authorization = 'Bearer ' + token;
        localStorage.setItem('token', token);
    }
    /*checkAdminRole(){
        const roles = this.checkForRoles();
        return roles !== null && roles.includes('Administrator')
    }*/ //todo
}