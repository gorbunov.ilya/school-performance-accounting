import axios from 'axios';
let token = localStorage.getItem("token");
const instance = axios.create({
    baseURL: 'https://localhost:44383/',
    headers: {'Authorization': `Bearer ${token}`},
    withCredentials: true
  });
export default instance;