import React, { Component } from 'react'
import axios from '../../services/axios';
import Container from '@material-ui/core/Container';
import Menu from '../../components/Menu';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Table, TableHead, TableRow, TableCell, TableBody, Button, TextField, InputLabel, Select, MenuItem, Input, ListItemText } from '@material-ui/core';
import '../../styles/usermanager.css';
import AuthService from '../../services/AuthService';
import User from '../../components/User';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import Search from '@material-ui/icons/Search';


class UserManager extends Component {

    constructor(props) {
        super(props);
        this.authService = new AuthService();
        this.state = {
            users: [],
            fullname: '',
            roles: [],
            selectedRole: 'Все роли',
            status: this.props.location.pathname.split('/'),
            isUpdateState: false
        }
    }

    componentDidMount() {
            this.authService.setAuthToken(localStorage.getItem('token'))
            axios.get('/api/usermanager')
                .then(response => this.setState({ users: response.data}))
            axios.get('api/roles')
                .then(response => this.setState({ roles: response.data }))
    }

    componentDidUpdate() {
        if (this.state.isUpdateState) {
          axios
            .get("/api/usermanager")
            .then(response => this.setState({ users: response.data }))
            .catch(error => console.log(error));
            
            this.setState({isUpdateState: false})
        }
    }

    handlerRefresh() {
        this.setState({ isUpdateState: true });
    }

    handleChange(event) {
        event.preventDefault();
        
    if(event.target.value === 'Все роли'){
        this.setState({selectedRole: 'Все роли'})
      }
      else{
        this.setState({ [event.target.name]: event.target.value });
    }
}

    userAddHandler() {
        this.props.history.push({
            pathname: '/add'
        })
    }

    submitSearch(event) {
        var roles = this.state.selectedRole
        event.preventDefault();
        if(roles === 'Все роли'){
            roles = []
        }
        axios.get('/api/usermanager?fullname=' 
        + this.state.fullname 
        + '&selectedRole=' +  roles)
            .then(response => this.setState({ users: response.data }))
            .catch(error => console.log(error))

    }

    render() {
        const { fullname, selectedRole} = this.state
        return (
            <div>
                <Menu />
                <Container component="main" maxWidth="lg" className="container">
                    <CssBaseline />
              <form onSubmit={this.submitSearch.bind(this)}>
                    <Grid container spacing={6}>
                    <Grid item xs={12} sm={3}>
                        <TextField
                            name="fullname"
                            variant="outlined"
                            id="fullname"
                            label="Поиск по ФИО"
                            value={fullname}
                            style={{marginTop: 7}}
                            onChange={this.handleChange.bind(this)}
                            fullWidth
                        />
                        </Grid>
                        <Grid item xs={12} sm={3}>
                        <FormControl fullWidth>
                        <InputLabel htmlFor="select-role">Фильтр по роли</InputLabel>
                        <Select
                            fullWidth
                            value={selectedRole}
                            name='selectedRole'
                            onChange={this.handleChange.bind(this)}
                            input={<Input id="select-role" />}
                        >
                            <MenuItem value={'Все роли'}>
                                    <ListItemText primary={'Все роли'} />
                                </MenuItem>
                            {this.state.roles.map(role =>
                                <MenuItem key={role} value={role}>
                                    <ListItemText primary={role} />
                                </MenuItem>
                            )}
                        </Select>
                        </FormControl>
                        </Grid>
                        <Grid item xs={6} sm={2}>
                        <Tooltip title="Поиск">
                            <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                fullWidth
                                style={{marginTop: 20}}
                            >
                               <Search/>
                            </Button>
                            </Tooltip>
                        </Grid>
                            <Grid item xs={6} sm={1}>
                            <Tooltip title="Добавить пользователя">
                                <Button
                                type="button"
                                onClick={this.userAddHandler.bind(this)}
                                variant="contained"
                                color="primary"
                                fullWidth
                                style={{marginTop: 20}}>
                                <AddIcon/>
                                </Button>
                                </Tooltip>
                            </Grid>
                            </Grid>
                    </form>
                    <Grid container spacing={4}>
                    <Grid item xs={12} sm={12}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Имя</TableCell>
                                        <TableCell>Email</TableCell>
                                        <TableCell>Роль</TableCell>
                                        <TableCell>Заблокирован</TableCell>
                                        <TableCell></TableCell>
                                        <TableCell>Действия</TableCell>
                                        <TableCell></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.users.map(item => (
                                        <User key={item.id} item={item} handlerRefresh={this.handlerRefresh.bind(this)}/>
                                    ))}
                                </TableBody>
                            </Table>
                        </Grid>
                        </Grid>
                </Container>
            </div>
        )
    }
}
export default UserManager;