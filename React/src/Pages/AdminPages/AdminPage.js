import React, { Component } from 'react';
import Menu from '../../components/Menu'
import Footer from '../../components/Footer';
import '../../styles/Home.css'
import AuthService from '../../services/AuthService';
import { Redirect } from 'react-router-dom';


class Home extends Component {
    constructor() {
        super();
        this.authService = new AuthService();
        }
    render() {
        const isAuthenticated = this.authService.isAuthenticated();
        return (
            !isAuthenticated ? <Redirect to ="/"/> : (
                <div id='#page-wrap'>
                <Menu />
                <div className='jumbotron content'>
                    <h3 className='text-center'>Учет успеваемости в школе</h3>
                    <p className='text-center'>Программа для учета четвертных оценок учащихся школы</p>
                    <p className='text-center'>Программа для создания отчета</p>
                </div>
                <Footer />
            </div>
            )
        )
    }
}
export default Home;