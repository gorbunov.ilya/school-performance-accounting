import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AuthService from '../../services/AuthService';
import PropTypes from 'prop-types';
import Snackbar from '../../components/Snackbar';
import { Redirect } from 'react-router-dom';
import superagent from 'superagent';
import theme from '../../styles/SignInStyles';

class SignIn extends Component {
  constructor() {
    super();
    this.authService = new AuthService();
    this.state = {
      email: "",
      password: ""
    }

  }
  handlerEmailChanged(event) {
    this.setState({ email: event.target.value })
  }
  handlerPasswordChanged(event) {
    this.setState({ passwordError: '' })
    this.setState({ errorPass: false })
    this.setState({ password: event.target.value })
  }
  submitForm(event) {
    event.preventDefault();
    if (this.authService.passwordValidator(this.state.password) === true) {
      superagent
        .post('https://localhost:44383/login')
        .send({ email: this.state.email, password: this.state.password })
        .end((err, res) => {
          if (err) {
            if (res.statusCode === 403) {
              this.setState({
                snackbarVariant: "error",
                snackbarMessage: "Ваша учетная запись заблокирована"
              });
              this.refs.child.handleOpen();
              return;
            }
            else{
              this.setState({
                snackbarVariant: "error",
                snackbarMessage: "Неверный логин или пароль"
              });
              this.refs.child.handleOpen();
              return;
            }
            
          }
          this.authService.login(res.body.token,res.body.roles)
          this.setState({});
        });
    } else {
      this.setState({
        passwordError:
          "Пароль должен состоять  как минимум  из 6 символов," +
          "из которых: 1 строчный,1 заглавный и 1 цифра",
        errorPass: true
      });
    }
  }
  render() {
    const { classes } = this.props;
    const isAuthenticated = this.authService.isAuthenticated();
    return (
      isAuthenticated ? <Redirect to={{ pathname: "/home" }} /> : (
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Snackbar ref='child' variant={this.state.snackbarVariant} message={this.state.snackbarMessage} />
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Авторизация
            </Typography>
            <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email"
                name="email"
                type="email"
                autoComplete="email"
                autoFocus
                value={this.state.email}
                onChange={this.handlerEmailChanged.bind(this)}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                value={this.state.password}
                type='password'
                onChange={this.handlerPasswordChanged.bind(this)}
                helperText={this.state.passwordError}
                error={this.state.errorPass}
                name="password"
                label="Пароль"
                id="password"
                autoComplete="current-password" />
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Запомнить меня?" />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit} >
                Войти
              </Button>
              <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Забыли пароль?
                  </Link>
                </Grid>
              </Grid>
            </form>
          </div>
        </Container>
      )
    );
  }
}
SignIn.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(theme)(SignIn);

