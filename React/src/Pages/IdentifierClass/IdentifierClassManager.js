import React, { Component } from "react";
import axios from "../../services/axios";
import Container from "@material-ui/core/Container";
import Menu from "../../components/Menu";
import CssBaseline from "@material-ui/core/CssBaseline";
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  TextField
} from "@material-ui/core";
import "../../styles/usermanager.css";
import AuthService from "../../services/AuthService";
import Grid from "@material-ui/core/Grid";
import IdentifierClass from "../../components/IdentifierClass";

class identifierClassManager extends Component {
  constructor(props) {
    super(props);
    this.authService = new AuthService();
    this.state = {
      identifierClasses: [],
      identifier: "",
      isUpdateState: false
    };
  }

  componentDidMount() {
    this.authService.setAuthToken(localStorage.getItem("token"));
    axios
      .get("/api/identifierClassManager")
      .then(response => this.setState({ identifierClasses: response.data }));
  }

  componentDidUpdate() {
    if (this.state.isUpdateState) {
      axios
        .get("/api/identifierClassManager")
        .then(response => this.setState({ identifierClasses: response.data }))
        .catch(error => console.log(error));

      this.setState({ isUpdateState: false });
    }
  }

  handlerRefresh() {
    this.setState({ isUpdateState: true });
  }

  handleChange(event) {
    event.preventDefault();

    this.setState({ [event.target.name]: event.target.value });
  }

  identifierClassAddHandler() {
    this.props.history.push({
      pathname: "/addidentifierclass"
    });
  }

  submitSearch(event) {
    event.preventDefault();
    axios
      .get(
        "/api/identifierClassManager?identifier=" +
          this.state.identifier
      )
      .then(response => this.setState({ identifierClasses: response.data }))
      .catch(error => console.log(error));
  }

  render() {
    const { identifier} = this.state;
    return (
      <div>
        <Menu />
        <Container component="main" maxWidth="lg" className="container">
          <CssBaseline />
          <Button
            type="button"
            onClick={this.identifierClassAddHandler.bind(this)}
            variant="contained"
            color="primary"
            style={{ marginBottom: 15 }}
          >
            Добавить идентификатор класса
          </Button>

          <form onSubmit={this.submitSearch.bind(this)}>
            <Grid container spacing={6}>
              <Grid item xs={12} sm={3}>
                <TextField
                  name="identifier"
                  variant="outlined"
                  id="identifier"
                  label="Ввод идентификатора"
                  value={identifier}
                  onChange={this.handleChange.bind(this)}
                  fullWidth
                />
              </Grid>              
              <Grid item xs={12} sm={2}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  fullWidth
                  className="submit-button"
                >
                  Поиск
                </Button>
              </Grid>
            </Grid>
          </form>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Идентификатор класса</TableCell>
                <TableCell>Действия</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.identifierClasses.map(item => (
                <IdentifierClass
                  key={item.id}
                  item={item}
                  handlerRefresh={this.handlerRefresh.bind(this)}
                />
              ))}
            </TableBody>
          </Table>
        </Container>
      </div>
    );
  }
}
export default identifierClassManager;
