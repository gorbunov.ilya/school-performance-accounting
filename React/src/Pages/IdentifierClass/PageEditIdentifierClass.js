import React from "react";
import FormHelperText from "@material-ui/core/FormHelperText";
import Avatar from "@material-ui/core/Avatar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Settings from "@material-ui/icons/Settings";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import PropTypes from "prop-types";
import axios from "../../services/axios";
import Menu from "../../components/Menu";
import AuthService from "../../services/AuthService";
import { Redirect } from "react-router-dom";
import Snackbar from "../../components/Snackbar";
import FormControl from "@material-ui/core/FormControl";
import EditFormTheme from "../../styles/PageCreateUserStyles";
import {
  Button,
  InputLabel,
  Select,
  MenuItem,
  Input,
  ListItemText
} from "@material-ui/core";

class PageEditIdentifierClass extends React.Component {
  constructor(props) {
    super(props);
    this.authService = new AuthService();
    this.state = {
      id: "",
      selectedParallel: "",
      selectedLetter: "",
      parallels: [],
      letters: [],
      identifierError: "",
      identifier: "",
      requestStatus: false,
      helperStatus: ""
    };
  }
  uri = this.props.location.pathname.split("/");

  componentDidMount() {
    axios
      .get("api/identifierClass/" + this.uri[2])
      .then(response =>
        this.setState({
          id: response.data.id,
          identifier: response.data.identifier
        })
      )
      .catch(error => console.log("Error:", error));

    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    const chars = [
      "А",
      "Б",
      "В",
      "Г",
      "Д",
      "Е",
      "Ё",
      "Ж",
      "З",
      "И",
      "Й",
      "К",
      "Л",
      "М",
      "Н",
      "О",
      "П",
      "Р",
      "С",
      "Т",
      "У",
      "Ф",
      "Х",
      "Ц",
      "Ч",
      "Ш",
      "Щ",
      "Ъ",
      "Э",
      "Ю",
      "Я"
    ];
    this.setState({ parallels: numbers });
    this.setState({ letters: chars });
  }

  handleChange(event) {
    event.preventDefault();
    this.setState({ [event.target.name]: event.target.value });
  }

  handleClick = event => {
    event.preventDefault();
    this.props.history.push("/identifierclassmanager");
  };
  submitForm(event) {
    event.preventDefault();
    if (this.state.selectedLetter === "" || this.state.selectedLetter === "") {
      this.setState({ identifierError: "Заполните все поля" });
    } else {
      axios
        .post("/api/editIdentifierClass", {
          id: this.state.id,
          parallel: this.state.selectedParallel,
          letter: this.state.selectedLetter
        })
        .then(response => response.data.result === true? 
          this.setState({snackbarMesssage:'Изменения сохранены.Вы будете перенаправлены.', 
            snackbarVariant: 'success'}, 
            () => this.refs.child.handleOpen(), 
            setTimeout(() => this.setState({ requestStatus: true }), 2000)) : null)
        .catch(error => this.setState(
          {
            snackbarMesssage:
              error.response.data.message,
            snackbarVariant: "error"
          },
          () => this.refs.child.handleOpen()
        ))
    }
  }
  render() {
    const isAuthenticated = this.authService.isAuthenticated();
    const { classes } = this.props;
    const {
      selectedParallel,
      selectedLetter,
      requestStatus,
      snackbarMesssage,
      snackbarVariant,
      identifierError
    } = this.state;
    return !isAuthenticated ? (
      <Redirect to="/" />
    ) : requestStatus ? (
      <Redirect to="/identifierclassmanager" />
    ) : (
      <div>
        <Menu />
        <Snackbar
          ref="child"
          message={snackbarMesssage}
          variant={snackbarVariant}
        />
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <Settings />
            </Avatar>
            <Typography component="h1" variant="h5">
              Редактирование
            </Typography>
            <Typography component="h1" variant="h6">
              {this.state.identifier}
            </Typography>
            <form
              className={classes.form}
              onSubmit={this.submitForm.bind(this)}
            >
              <Grid container spacing={6}>
                <Grid item xs={12} sm={4}>
                  <FormControl fullWidth required>
                    <InputLabel htmlFor="select-parallel">Цифра</InputLabel>
                    <Select
                      fullWidth
                      value={selectedParallel}
                      name="selectedParallel"
                      onChange={this.handleChange.bind(this)}
                      input={<Input id="select-parallel" />}
                    >
                      {this.state.parallels.map(parallel => (
                        <MenuItem key={parallel} value={parallel}>
                          <ListItemText primary={parallel} />
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={4}>
                  <FormControl fullWidth required>
                    <InputLabel htmlFor="select-letter">Буква</InputLabel>
                    <Select
                      fullWidth
                      value={selectedLetter}
                      name="selectedLetter"
                      onChange={this.handleChange.bind(this)}
                      input={<Input id="select-letter" />}
                    >
                      {this.state.letters.map(letter => (
                        <MenuItem key={letter} value={letter}>
                          <ListItemText primary={letter} />
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
              </Grid>
              <FormHelperText error>{identifierError}</FormHelperText>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                >
                  Изменить
                </Button>
                <Button
                  onClick={this.handleClick}
                  fullWidth
                  variant="contained"
                  color="primary"
                >
                  Отменить
                </Button>
            </form>
          </div>
        </Container>
      </div>
    );
  }
}
PageEditIdentifierClass.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(EditFormTheme)(PageEditIdentifierClass);
