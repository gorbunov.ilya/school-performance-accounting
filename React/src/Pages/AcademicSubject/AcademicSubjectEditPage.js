import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import PropTypes from 'prop-types';
import axios from '../../services/axios';
import Menu from '../../components/Menu';
import AuthService from '../../services/AuthService';
import { Redirect } from 'react-router-dom';
import Snackbar from '../../components/Snackbar';
import EditFormTheme from '../../styles/PageCreateUserStyles';
import Book from '@material-ui/icons/Book';

class AcademicSubjectEdit extends React.Component {
  constructor(props) {
    super(props);
    this.authService = new AuthService();
    this.state = {
      id: '',
      name: '',
      requestStatus: false,
      error: []
    }
  }
   uri = this.props.location.pathname.split('/')

  componentDidMount() {
      axios.get('api/academicSubject/' + this.uri[2])
        .then(response => this.setState({
          name: response.data.name,
          id: response.data.id
        }))
        .catch(error => console.log('Error:', error))
  }
  handleChange(event) {
    event.preventDefault();
    this.setState({ [event.target.name]: event.target.value });
  }
  handleClick = event => {
    event.preventDefault();
    this.props.history.push('/subjectsmanager');
  }
  submitForm(event) {
    event.preventDefault();
          axios.post('api/editAcademicSubject', {
            name: this.state.name,
            id: this.state.id
          })
            .then(response => response.data.result === true? this.setState({snackbarMesssage:'Изменения сохранены.Вы будете перенаправлены.', 
            snackbarVariant: 'success'}, () => this.refs.child.handleOpen(), setTimeout(() => this.setState({ requestStatus: true }), 2000)) : null)
            .catch(error =>
              this.setState({
                snackbarMesssage: error.response.data.message,
                snackbarVariant: 'error',
                error : error
              }, () => this.refs.child.handleOpen()))
    
  }
  render() {
    const isAuthenticated = this.authService.isAuthenticated();
    const { classes } = this.props;
    const {name, requestStatus,  snackbarMesssage, 
            snackbarVariant} = this.state
    return (
      !isAuthenticated ? <Redirect to="/" /> : (
        requestStatus ? <Redirect to='/subjectsmanager'/> : (
          <div>
          <Menu />
          <Snackbar ref='child' message={snackbarMesssage} variant={snackbarVariant} />
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
              <Avatar className={classes.avatar}>
                <Book />
              </Avatar>
              <Typography component="h1" variant="h5">
                Редактирование
              </Typography>
              <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                <Grid container spacing={1}>
                  <Grid item xs={12}>
                    <TextField
                      name="name"
                      variant="outlined"
                      required
                      fullWidth
                      id="name"
                      label="Название предмета"
                      value={name}
                      onChange={this.handleChange.bind(this)}
                      autoFocus/>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  >
                  Изменить
                </Button>
                </Grid>
                <Grid item xs={12} sm={6}>
                <Button
                  onClick={this.handleClick}
                  fullWidth
                  variant="contained"
                  className={classes.submit}
                  >
                  Отменить
                </Button>
                </Grid>
                </Grid>
              </form>
            </div>
          </Container>
        </div>
        )
      )
    );
  }
}
AcademicSubjectEdit.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(EditFormTheme)(AcademicSubjectEdit);