import React, { Component } from 'react'
import axios from '../../services/axios';
import Container from '@material-ui/core/Container';
import Menu from '../../components/Menu';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Table, TableHead, TableRow, TableCell, TableBody, Button, TextField} from '@material-ui/core';
import '../../styles/usermanager.css';
import AuthService from '../../services/AuthService';
import Grid from '@material-ui/core/Grid';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import Search from '@material-ui/icons/Search';
import Year from '../../components/Year';

class AcademicYears extends Component {

    constructor(props) {
        super(props);
        this.authService = new AuthService();
        this.state = {
            years: [],
            name: '',
            isUpdateState: false
        }
    }

    componentDidMount() {
            this.authService.setAuthToken(localStorage.getItem('token'))
            axios.get('api/academicYearManager')
                .then(response => this.setState({ years: response.data }))
    }

    componentDidUpdate() {
        if (this.state.isUpdateState) {
          axios
            .get("api/academicYearManager")
            .then(response => this.setState({ years: response.data }))
            .catch(error => console.log(error));
            
            this.setState({isUpdateState: false})
        }
    }

    handlerRefresh() {
        this.setState({ isUpdateState: true });
    }
    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
}

    userAddHandler() {
        this.props.history.push({
            pathname: '/add-year'
        })
    }

    submitSearch(event) {
        event.preventDefault();
        axios.get('api/academicYearManager?name=' + this.state.name)
            .then(response => this.setState({ years: response.data }))
            .catch(error => console.log(error))

    }

    render() {
        const { name } = this.state
        return (
            <div>
                <Menu />
                <Container component="main" maxWidth="lg" className="container">
                    <CssBaseline />
              <form onSubmit={this.submitSearch.bind(this)}>
                    <Grid container spacing={4}>
                    <Grid item xs={12} sm={3}>
                        <TextField
                            name="name"
                            variant="outlined"
                            id="name"
                            label="Поиск по названию"
                            value={name}
                            onChange={this.handleChange.bind(this)}
                            fullWidth
                        />
                        </Grid>
                        <Grid item xs={12} sm={2}>
                        <Tooltip title="Поиск">
                            <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                fullWidth
                                className='submit-button'
                                style={{marginTop: 6}}
                            >
                                <Search/>
                            </Button>
                          </Tooltip>
                        </Grid>
                            <Grid item xs={12} sm={5}>
                            <Tooltip title="Добавить год">
                                <Button
                                type="button"
                                onClick={this.userAddHandler.bind(this)}
                                variant="contained"
                                color="primary"
                                style={{marginTop: 6}}
                                >
                                <AddIcon/>
                                </Button>
                                </Tooltip>
                            </Grid>
                        </Grid>
                    </form>
                    <Grid container spacing={4}>
                    <Grid item xs={12} sm={12}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Название учебного года</TableCell>
                                <TableCell>Дата начала(гг-мм-дд)</TableCell>
                                <TableCell>Дата окончания(гг-мм-дд)</TableCell>
                                <TableCell>Текущий</TableCell>
                                <TableCell align="right">Действия</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.years.map(item => (
                                <Year key={item.id} item={item} handlerRefresh={this.handlerRefresh.bind(this)}/>
                            ))}
                        </TableBody>
                    </Table>
                    </Grid>
                    </Grid>
                </Container>
            </div>
        )
    }
}
export default AcademicYears;