import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Book from '@material-ui/icons/Book';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import PropTypes from 'prop-types';
import axios from '../../services/axios';
import Menu from '../../components/Menu';
import AuthService from '../../services/AuthService';
import Checkbox from '@material-ui/core/Checkbox';
import { Redirect } from 'react-router-dom';
import Snackbar from '../../components/Snackbar';
import CreateFormTheme from '../../styles/PageCreateUserStyles';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';


class AcademicYearCreate extends React.Component {
    constructor(props) {
        super(props);
        this.authService = new AuthService();
        this.state = {
            name: '',
            beginDate : new Date(),
            endDate: new Date(),
            checked: false,
            isCurrent: false
        }
    }

    handleChange(event) {
        
        if(event.target.name === 'isCurrent')
        {
            this.setState({ [event.target.name]: !this.state.isCurrent });
            this.setState({ checked: !this.state.checked });
        }
        else{
            event.preventDefault();
            this.setState({ [event.target.name]: event.target.value });
        }
        
    }
    handleBeginDateChange(date) {
        this.setState({beginDate : date});
    }
    handleEndDateChange(date) {
        this.setState({endDate : date});
    }
    handleClick = event => {
        event.preventDefault();
        this.props.history.push('/yearsmanager');
    }
    submitForm(event) {
        event.preventDefault();
        axios.post('api/createAcademicYear', {
            name: this.state.name,
            beginDate: this.state.beginDate,
            endDate: this.state.endDate,
            isCurrent: this.state.isCurrent
        })
        .then(response => response.data.result === true? this.setState({snackbarMesssage:'Изменения сохранены.Вы будете перенаправлены.', 
        snackbarVariant: 'success'}, () => this.refs.child.handleOpen(), setTimeout(() => this.setState({ requestStatus: true }), 2000)) : null)
            .catch(error =>
                this.setState({
                    snackbarMesssage: error.response.data.message,
                    snackbarVariant: 'error'
                }, () => this.refs.child.handleOpen()))
    }
    render() {
        const isAuthenticated = this.authService.isAuthenticated();
        const { classes } = this.props; 
        const { name ,requestStatus, snackbarMesssage, 
            snackbarVariant, beginDate, endDate,checked } = this.state

        return (
            !isAuthenticated ? <Redirect to="/" /> : (
                requestStatus ? <Redirect to='/yearsmanager' /> : (
                    <div>
                        <Menu />
                        <Snackbar ref='child' message={snackbarMesssage} variant={snackbarVariant} />
                        <Container component="main" maxWidth="xs">
                            <CssBaseline />
                            <div className={classes.paper}>
                                <Avatar className={classes.avatar}>
                                    <Book />
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Добавление учебного года
                                    </Typography>
                                <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                                    <Grid container spacing={1}>
                                        <Grid item xs={12}>
                                            <TextField
                                                name="name"
                                                variant="outlined"
                                                required
                                                fullWidth
                                                id="name"
                                                label="Название учебного года"
                                                value={name}
                                                onChange={this.handleChange.bind(this)}
                                                autoFocus
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker
                                                fullWidth
                                                disableToolbar
                                                variant="inline"
                                                format="MM/dd/yyyy"
                                                margin="normal"
                                                id="beginDate"
                                                name='beginDate'
                                                label="Дата начала"
                                                value={beginDate}
                                                onChange={this.handleBeginDateChange.bind(this)}
                                                KeyboardButtonProps={{
                                                    'aria-label': 'выберите дату',
                                                }}
                                                />
                                        </MuiPickersUtilsProvider>
                                        </Grid>
                                        <Grid item xs={12}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardDatePicker
                                        disableToolbar
                                        variant="inline"
                                        fullWidth
                                        format="MM/dd/yyyy"
                                        margin="normal"
                                        id="beginDate"
                                        name='endDate'
                                        label="Дата окончания"
                                        value={endDate}
                                        onChange={this.handleEndDateChange.bind(this)}
                                        KeyboardButtonProps={{
                                            'aria-label': 'выберите дату',
                                        }}
                                        />
                                        </MuiPickersUtilsProvider>
                                        </Grid>
                                        <Grid item xs={12}>
                                        <FormGroup row>
                                        <FormControlLabel
                                        control={
                                        

                                        <Checkbox
                                            checked={checked}
                                            onChange={this.handleChange.bind(this)}
                                            color="primary"
                                            name="isCurrent"
                                            inputProps={{
                                            'aria-label': 'Текущий?',
                                            }}
                                        />}
                                        label="Текущий учебный год?" />
                                        </FormGroup>
                                        </Grid>
                                        
                                    <Grid item xs={12} sm={6}>
                                    <Button
                                        type="submit"
                                        fullWidth
                                        variant="contained"
                                        color="primary"
                                        className={classes.submit}>
                                        Добавить
                                        </Button>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                    <Button
                                        onClick={this.handleClick}
                                        fullWidth
                                        variant="contained"
                                        className={classes.submit}>
                                        Отменить
                                         </Button>
                                         </Grid>
                                         </Grid>
                                </form>
                            </div>
                        </Container>
                    </div>
                )
            )
        );
    }
}
AcademicYearCreate.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(CreateFormTheme)(AcademicYearCreate);