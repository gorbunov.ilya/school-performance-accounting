import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Settings from '@material-ui/icons/Settings';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import PropTypes from 'prop-types';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import axios from '../../services/axios';
import Input from '@material-ui/core/Input';
import Menu from '../../components/Menu';
import AuthService from '../../services/AuthService';
import { Redirect } from 'react-router-dom';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Snackbar from '../../components/Snackbar';
import FormHelperText from '@material-ui/core/FormHelperText';
import EditFormTheme from '../../styles/PageCreateUserStyles';

class EditForm extends React.Component {
  constructor(props) {
    super(props);
    this.authService = new AuthService();
    this.state = {
      id: '',
      roles: [],
      selectedRole: [],
      firstName: '',
      surname: '',
      middlename: '',
      email: '',
      password: '',
      isBlocked: false,
      requestStatus: false,
      roleError: '',
      helperStatus: ''
    }
  }
  uri = this.props.location.pathname.split('/')

  componentDidMount() {
    axios.get('api/user/' + this.uri[2])
      .then(response => this.setState({
        id: response.data.id,
        email: response.data.email,
        roles: response.data.roles,
        password: response.data.password,
        surname: response.data.surname,
        middlename: response.data.middleName,
        firstName: response.data.name,
        selectedRole: response.data.userRoles
      }))
      .catch(error => console.log('Error:', error))
  }
  handleChange(event) {
    event.preventDefault();
    if (event.target.name === 'selectedRole' && event.target.value !== null) {
      this.setState({ roleError: '' })
    }
    this.setState({ [event.target.name]: event.target.value });
  }
  handleClick = event => {
    event.preventDefault();
    this.props.history.push('/usermanager');
  }
  submitForm(event) {
    event.preventDefault();
    if (this.state.selectedRole.length > 0) {
      axios.post('/api/edit', {
        id: this.state.id,
        name: this.state.firstName,
        surname: this.state.surname,
        email: this.state.email,
        password: this.state.password,
        roles: this.state.selectedRole,
        middlename: this.state.middlename,
        isblocked: this.state.isBlocked
      })
      .then(response => response.data.result === true? this.setState({snackbarMesssage:'Изменения сохранены.Вы будете перенаправлены.', 
      snackbarVariant: 'success'}, () => this.refs.child.handleOpen(), setTimeout(() => this.setState({ requestStatus: true }), 2000)) : null)
        .catch(error =>
          this.setState({
            snackbarMesssage: 'Данный email уже используется',
            snackbarVariant: 'error'
          }, () => this.refs.child.handleOpen()))
    }
    else {
      this.setState({ roleError: 'Выберите роль' })
    }
  }
  render() {
    const isAuthenticated = this.authService.isAuthenticated();
    const { classes } = this.props;
    const { email, firstName,
      surname, middlename, selectedRole,
      requestStatus, roleError, snackbarMesssage,
      snackbarVariant } = this.state
    return (
      !isAuthenticated ? <Redirect to="/" /> : (
        requestStatus ? <Redirect to='/usermanager/result=ok' /> : (
          <div>
            <Menu />
            <Snackbar ref='child' message={snackbarMesssage} variant={snackbarVariant} />
            <Container component="main" maxWidth="xs">
              <CssBaseline />
              <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                  <Settings />
                </Avatar>
                <Typography component="h1" variant="h5">
                  Редактирование
              </Typography>
                <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                  <Grid container spacing={1}>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        name="firstName"
                        variant="outlined"
                        required
                        fullWidth
                        id="firstName"
                        label="Имя"
                        value={firstName}
                        onChange={this.handleChange.bind(this)}
                        autoFocus />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="surname"
                        label="Фамилия"
                        name="surname"
                        value={surname}
                        onChange={this.handleChange.bind(this)} />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        variant="outlined"
                        fullWidth
                        id="middlename"
                        label="Отчество"
                        name="middlename"
                        value={middlename}
                        onChange={this.handleChange.bind(this)} />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="email"
                        type='email'
                        label="Email(Логин)"
                        name="email"
                        value={email}
                        onChange={this.handleChange.bind(this)} />
                    </Grid>
                    <Grid item xs={12}>
                      <FormControl className={classes.formControl} fullWidth required>
                        <InputLabel htmlFor="select-multiple-checkbox">Роль</InputLabel>
                        <Select
                          multiple
                          value={selectedRole}
                          name='selectedRole'
                          onChange={this.handleChange.bind(this)}
                          input={<Input id="select-multiple-checkbox" />}
                          renderValue={selected => selected.join(', ')}>
                          {this.state.roles.map(role =>
                            <MenuItem key={role} value={role}>
                              <Checkbox checked={selectedRole.indexOf(role) > -1} />
                              <ListItemText primary={role} />
                            </MenuItem>
                          )}
                        </Select>
                        <FormHelperText error>{roleError}</FormHelperText>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Button
                        fullWidth
                        type="submit"
                        variant="contained"
                        color="primary"
                        className={classes.submit}>
                        Изменить
                      </Button>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Button
                        fullWidth
                        onClick={this.handleClick}
                        className={classes.submit}
                        variant="contained">
                        Отменить
                       </Button>
                    </Grid>
                  </Grid>
                </form>
              </div>
            </Container>
          </div>
        )
      )
    );
  }
}
EditForm.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(EditFormTheme)(EditForm);