import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Settings from '@material-ui/icons/Settings';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import PropTypes from 'prop-types';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import axios from '../../services/axios';
import Input from '@material-ui/core/Input';
import Menu from '../../components/Menu';
import AuthService from '../../services/AuthService';
import { Redirect } from 'react-router-dom';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Snackbar from '../../components/Snackbar';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import CreateFormTheme from '../../styles/PageCreateUserStyles';
import generatePassword from 'generate-password'
class CreateForm extends React.Component {
  constructor(props) {
    super(props);
    this.authService = new AuthService();
    this.state = {
      roles: [],
      selectedRole: [],
      firstName: '',
      surname: '',
      middlename: '',
      email: '',
      password: '',
      showPassword: true,
      isBlocked: false,
      requestStatus: false,
      roleError: ''
    }
  }
  uri = this.props.location.pathname.split('/')

  componentDidMount() {
    axios.get('/api/roles')
      .then(response => this.setState({ roles: response.data }))
      .catch(error => console.log('Error:', error));
  }
  handleChange(event) {
    event.preventDefault();
    if (event.target.name === 'selectedRole') {
      this.setState({ roleError: '' })
    }
    this.setState({ [event.target.name]: event.target.value });
  }
  generatePassword = () => {
    var password = generatePassword.generate({
      length: 6,
      numbers: true,
      strict: true
    });
    this.setState({ password: password })
  }
  handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };
  handleMouseDownPassword = event => {
    event.preventDefault();
  };
  handleClick = event => {
    event.preventDefault();
    this.props.history.push('/usermanager');
  }
  submitForm(event) {
    event.preventDefault();
    if (this.authService.passwordValidator(this.state.password) === true) {
      if (this.state.selectedRole.length > 0) {
        axios.post('/api/add', {
          name: this.state.firstName,
          surname: this.state.surname,
          email: this.state.email,
          password: this.state.password,
          roles: this.state.selectedRole,
          middlename: this.state.middlename,
          isblocked: this.state.isBlocked
        })
        .then(response => response.data.result === true? this.setState({snackbarMesssage:'Изменения сохранены.Вы будете перенаправлены.', 
        snackbarVariant: 'success'}, () => this.refs.child.handleOpen(), setTimeout(() => this.setState({ requestStatus: true }), 2000)) : null)
          .catch(error =>
            this.setState({
              snackbarMesssage: 'Данный email уже используется',
              snackbarVariant: 'error'
            }, () => this.refs.child.handleOpen()))
      }
      else {
        this.setState({ roleError: 'Выберите роль' })
      }
    }
    else {
      this.setState({
        passwordError: 'Пароль должен состоять  как минимум  из 6 латинских символов,' +
          'из которых: 1 строчный,1 заглавный и 1 цифра', errorPass: true
      });
    }
  }
  render() {
    const isAuthenticated = this.authService.isAuthenticated();
    const { classes } = this.props;
    const { email, password, firstName,
      surname, middlename, selectedRole,
      passwordError, errorPass, showPassword, requestStatus, roleError, snackbarMesssage, snackbarVariant } = this.state

    return (
      !isAuthenticated ? <Redirect to="/" /> : (
        requestStatus ? <Redirect to='/usermanager/result=ok' /> : (
          <div>
            <Menu />
            <Snackbar ref='child' message={snackbarMesssage} variant={snackbarVariant} />
            <Container component="main" maxWidth="xs">
              <CssBaseline />
              <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                  <Settings />
                </Avatar>
                <Typography component="h1" variant="h5">
                  Добавление пользователя
              </Typography>
                <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                  <Grid container spacing={1}>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        name="firstName"
                        variant="outlined"
                        required
                        fullWidth
                        id="firstName"
                        label="Имя"
                        value={firstName}
                        onChange={this.handleChange.bind(this)}
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="surname"
                        label="Фамилия"
                        name="surname"
                        value={surname}
                        onChange={this.handleChange.bind(this)}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        variant="outlined"
                        fullWidth
                        id="middlename"
                        label="Отчество"
                        name="middlename"
                        value={middlename}
                        onChange={this.handleChange.bind(this)}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        variant="outlined"
                        required
                        fullWidth
                        id="email"
                        type='email'
                        label="Email(Логин)"
                        name="email"
                        value={email}
                        onChange={this.handleChange.bind(this)}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <FormControl className={classes.formControl} fullWidth required>
                        <InputLabel htmlFor="select-multiple-checkbox">Роль</InputLabel>
                        <Select
                          multiple
                          value={selectedRole}
                          name='selectedRole'
                          onChange={this.handleChange.bind(this)}
                          input={<Input id="select-multiple-checkbox" />}
                          renderValue={selected => selected.join(', ')}>
                          {this.state.roles.map(role =>
                            <MenuItem key={role} value={role}>
                              <Checkbox checked={selectedRole.indexOf(role) > -1} />
                              <ListItemText primary={role} />
                            </MenuItem>
                          )}
                        </Select>
                        <FormHelperText error>{roleError}</FormHelperText>
                      </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={7}>
                      <TextField
                        variant="outlined"
                        required
                        fullWidth
                        name="password"
                        label="Пароль"
                        type={showPassword ? 'text' : 'password'}
                        id="password"
                        helperText={passwordError}
                        error={errorPass}
                        value={password}
                        onChange={this.handleChange.bind(this)}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                edge="end"
                                aria-label="toggle password visibility"
                                onClick={this.handleClickShowPassword}
                                onMouseDown={this.handleMouseDownPassword}
                              >
                                {showPassword ? <VisibilityOff /> : <Visibility />}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }} />
                    </Grid>
                    <Grid item xs={12} sm={5}>
                      <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        onClick={this.generatePassword}
                        className={classes.generate}>
                        Сгенерировать
                      </Button>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                      className={classes.submit}>
                      Добавить
                    </Button>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Button
                      onClick={this.handleClick}
                      fullWidth
                      className={classes.submit}
                      variant="contained">
                      Отменить
                    </Button>
                  </Grid>
                  </Grid>
                </form>
              </div>
            </Container>
          </div>
        )
      )
    );
  }
}
CreateForm.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(CreateFormTheme)(CreateForm);