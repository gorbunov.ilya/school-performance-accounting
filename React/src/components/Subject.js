import React, { Component } from 'react'
import { TableRow, TableCell, Button } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import DeleteModal from './Modals/DeleteModal';
import '../styles/User.css';
import Create from '@material-ui/icons/Create';
import Tooltip from '@material-ui/core/Tooltip';
class Subject extends Component {

    state = {
        id: this.props.item.id,
        action: null
    }


    actionHandler(event, id, action) {
        event.preventDefault();
        this.props.history.push('/' + action + '/' + id)
    }

    render() {

        return (
            <TableRow key={this.props.item.id}>
                <TableCell>{this.props.item.name}</TableCell>
                <TableCell
                    className="buttonPadding"
                    align="right"
                >
                        <Tooltip title="Редактировать">
                    <Button
                        type="button"
                        variant="contained"
                        color="primary"
                        onClick={event => this.actionHandler(event, this.props.item.id, 'edit-subject')}
                    ><Create/>
                    </Button>
                    </Tooltip>
                </TableCell>
                <TableCell
                    align="left"
                    className="buttonPadding"
                >
                        <DeleteModal id={this.props.item.id} handlerRefresh={this.props.handlerRefresh} path='api/deleteAcademicSubject'/>                   
                </TableCell>
            </TableRow>
        )
    }
}
export default withRouter(Subject);