import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import axios from "../../services/axios";
import DeleteIcon from '@material-ui/icons/Delete';
import Tooltip from '@material-ui/core/Tooltip';
class DeleteUserModal extends Component {
  constructor() {
    super();
    this.state = {
      open: false
    };
  }
  handleClickOpen(event) {
    event.preventDefault();
    this.setState({ open: true });
  }

  handleClose(event) {
    event.preventDefault();
    this.setState({ open: false });
  }

  deleteHandler(event) {
    event.preventDefault();
    this.setState({ open: false });
    axios
      .post(this.props.path, null, {
        params: {
          id: this.props.id
        }
      })
      .then(response => {
        this.props.handlerRefresh();
      })
      .catch(err => console.warn(err));
  }
  render() {
    return (
      <div>
        <Tooltip title='Удалить'>
        <Button
          type="button"
          variant="contained"
          color="secondary"
          onClick={this.handleClickOpen.bind(this)}
        >
          <DeleteIcon />
        </Button>
        </Tooltip>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose.bind(this)}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title" />
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Вы уверены что хотите удалить?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose.bind(this)} color="primary" autoFocus>
              Нет
            </Button>
            <Button
              onClick={this.deleteHandler.bind(this)}
              color="primary"
            >
              Да
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default DeleteUserModal;
