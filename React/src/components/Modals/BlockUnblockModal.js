import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import axios from "../../services/axios";
import Tooltip from '@material-ui/core/Tooltip';
class BlockUnblockUserModal extends Component {
  constructor() {
    super();
    this.state = {
      open: false
    };
  }
  handleClickOpen(event) {
    event.preventDefault();
    this.setState({ open: true });
  }

  handleClose(event) {
    event.preventDefault();
    this.setState({ open: false });
  }

  blockHandler(event) {
    event.preventDefault();
    this.setState({ open: false });
    axios
      .post("/api/blockunblock", null, {
        params: {
          id: this.props.id
        }
      })
      .then(response => {
        this.props.handlerRefresh();
      })
      .catch(err => console.warn(err));
  }
  render() {
    return (
      <div>
        <Tooltip title={this.props.name[0]}>
        <Button
          type="button"
          variant="contained"
          color="primary"
          onClick={this.handleClickOpen.bind(this)}
        >
          {this.props.name[1]}
        </Button>
        </Tooltip>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose.bind(this)}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title" />
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Вы уверены что хотите {this.props.name[0]} пользователя?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose.bind(this)} color="primary">
              Нет
            </Button>
            <Button
              onClick={this.blockHandler.bind(this)}
              color="primary"
              autoFocus
            >
              Да
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default BlockUnblockUserModal;
