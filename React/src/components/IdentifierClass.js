import React, { Component } from 'react'
import { TableRow, TableCell, Button} from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import Create from '@material-ui/icons/Create';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteModal from './Modals/DeleteModal';
class IdentifierClass extends Component {

    state = {
        id: this.props.item.id,
        action: null
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
        this.setState({action: event.target.value})
    }

    actionHandler(event, id, action){
        event.preventDefault();
        this.props.history.push('/'+ action +'/' + id)
    }

    render() {
        return (
            <TableRow key={this.props.item.id}>
                <TableCell>{this.props.item.identifier}</TableCell>
                
                <TableCell
                    align="right"
                    >
                        <Tooltip title="Редактировать">
                    <Button
                        type="button"
                        variant="contained"
                        color="primary"
                        onClick={event => this.actionHandler(event, this.props.item.id, 'editidentifierclass')}
                    ><Create/>
                    </Button>
                    </Tooltip>
                </TableCell>
                <TableCell
                    align="left"
                    >
                         <DeleteModal id={this.props.item.id} handlerRefresh={this.props.handlerRefresh} path='api/deleteIdentifierClass'/>                   
                </TableCell>  
            </TableRow>
        )
    }
}
export default withRouter (IdentifierClass);