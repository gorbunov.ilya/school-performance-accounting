import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import { NavLink, withRouter} from 'react-router-dom'

 class SideBar extends React.Component {
  render() {
    return (
      <Menu>
        <NavLink className="menu-item" to='/usermanager'>
          Управление пользователями
        </NavLink>
        <NavLink className="menu-item" to='/subjectsmanager'>
          Управление предметами
          </NavLink>
        <NavLink className="menu-item" to='/identifierclassmanager'>
          Управление идентификаторами классов
        </NavLink>
        <NavLink className="menu-item" to='/yearsmanager'>
          Управление учебными годами
        </NavLink>
      </Menu>
    )
  }
}
export default withRouter(SideBar)
