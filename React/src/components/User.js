import React, { Component } from 'react'
import { TableRow, TableCell, Button} from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import Clear from '@material-ui/icons/Clear';
import Block from '@material-ui/icons/Lock';
import Unblock from '@material-ui/icons/LockOpen';
import Create from '@material-ui/icons/Create';
import CheckCircle from '@material-ui/icons/CheckCircle';
import BlockUnblockUserModal from './Modals/BlockUnblockModal';
import DeleteUserModal from './Modals/DeleteModal';
import Tooltip from '@material-ui/core/Tooltip';
import '../styles/User.css';
class User extends Component {

    state = {
        id: this.props.item.id,
        action: null
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
        this.setState({action: event.target.value})
    }

    actionHandler(event, id, action){
        event.preventDefault();
        this.props.history.push('/'+ action +'/' + id)
    }

    render() {
        const IsBlocked = this.props.item.isBlocked; 
        var buttonName;       
        if (IsBlocked) {
            buttonName = ['Разблокировать',<Unblock/>] 
        }
        else{
            buttonName = ['Заблокировать',<Block/>]
        }
        return (
            <TableRow key={this.props.item.id}>
                <TableCell>{this.props.item.fullName}</TableCell>
                <TableCell>{this.props.item.email}</TableCell>
                <TableCell >{this.props.item.rolesName.join(', ')}</TableCell>
                <TableCell variant='footer'>
                {this.props.item.isBlocked  ? <CheckCircle/>: <Clear/> }
                </TableCell>
                <TableCell
                    className="buttonPadding"
                    align="right"
                    >
                        <Tooltip title="Редактировать">
                    <Button
                        type="button"
                        variant="contained"
                        onClick={event => this.actionHandler(event, 
                            this.props.item.id, 'edit')}
                    ><Create/>
                    </Button>
                    </Tooltip>
                </TableCell>
                <TableCell
                    className="buttonPadding"
                    align="center"
                    >   
                        <BlockUnblockUserModal 
                        id={this.props.item.id} 
                        name={buttonName} 
                        handlerRefresh={this.props.handlerRefresh}/>
                                    
                </TableCell>
                <TableCell
                    className="buttonPadding"
                    align="left"
                    >   
                        <DeleteUserModal 
                        id={this.props.item.id} 
                        handlerRefresh={this.props.handlerRefresh} 
                        path='api/delete'/> 
                </TableCell>  
            </TableRow>
        )
    }
}
export default withRouter (User);