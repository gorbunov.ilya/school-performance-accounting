import React, { Component } from 'react'
import { TableRow, TableCell, Button } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import DeleteModal from './Modals/DeleteModal';
import '../styles/User.css';
import Create from '@material-ui/icons/Create';
import Tooltip from '@material-ui/core/Tooltip';
import Clear from '@material-ui/icons/Clear';
import CheckCircle from '@material-ui/icons/CheckCircle';

class Year extends Component {

    state = {
        id: this.props.item.id,
        action: null
    }


    actionHandler(event, id, action) {
        event.preventDefault();
        this.props.history.push('/' + action + '/' + id)
    }

    render() {
        return (
            <TableRow key={this.props.item.id}>
                <TableCell>{this.props.item.name}</TableCell>
                <TableCell>{this.props.item.beginDate.split('T')[0]}</TableCell>
                <TableCell>{this.props.item.endDate.split('T')[0]}</TableCell>
                <TableCell>{this.props.item.isCurrent ? <CheckCircle/>: <Clear/>}</TableCell>
                <TableCell
                    className="buttonPadding"
                    align="right"
                >
                        <Tooltip title="Редактировать">
                    <Button
                        type="button"
                        variant="contained"
                        color="primary"
                        onClick={event => this.actionHandler(event, this.props.item.id, 'edit-year')}
                    ><Create/>
                    </Button>
                    </Tooltip>
                </TableCell>
                <TableCell
                    align="left"
                    className="buttonPadding"
                >
                        <DeleteModal id={this.props.item.id} handlerRefresh={this.props.handlerRefresh} path='api/deleteAcademicYear'/>                   
                </TableCell>
            </TableRow>
        )
    }
}
export default withRouter(Year);