﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.AcademicYearDtos
{
    public class AcademicYearDto : BaseEntityDto
    {
        public string Name { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsCurrent { get; set; }
    }
}
