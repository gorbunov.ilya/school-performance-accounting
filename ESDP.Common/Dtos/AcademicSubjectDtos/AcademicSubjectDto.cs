﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.AcademicSubjectDtos
{
    public class AcademicSubjectDto : BaseEntityDto
    {
        public string Name { get; set; }
    }
}
