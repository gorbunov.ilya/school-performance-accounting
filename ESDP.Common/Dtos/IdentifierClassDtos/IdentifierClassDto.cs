﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.IdentifierClassDtos
{
    public class IdentifierClassDto : BaseEntityDto
    {
        public string Identifier { get; set; }
    }
}
