﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.IdentifierClassDtos
{
    public class CreateIdentifierClassDto : BaseEntityDto
    {
        public int Parallel { get; set; }
        public char Letter { get; set; }
    }
}
