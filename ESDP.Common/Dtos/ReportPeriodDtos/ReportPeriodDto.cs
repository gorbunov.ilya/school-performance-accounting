﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.ReportPeriodDtos
{
    public class ReportPeriodDto : BaseEntityDto
    {
        public string Name { get; set; }
    }
}
