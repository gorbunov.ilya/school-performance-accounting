﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.ReportPeriodDtos
{
    public class ReportPeriodFilterDto
    {
        public string Name { get; set; }
    }
}
