﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.UserDtos
{
    public class AuthorizeUserDto
    {
        public string Token { get; set; }
        public IList<string> Roles { get; set; }
    }
}
