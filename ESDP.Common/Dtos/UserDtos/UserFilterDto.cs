﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Common.Dtos.UserDtos
{
    public class UserFilterDto
    {
        public string Fullname { get; set; }
        public string SelectedRole { get; set; }
    }
}
