﻿using ESDP.BL.Services;
using ESDP.Common.Dtos.AcademicYearDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator, Director, Headteacher")]
    public class AcademicYearController : Controller
    {
        private readonly AcademicYearService _academicYearService;

        public AcademicYearController(AcademicYearService service)
        {
            _academicYearService = service;
        }

        [Route("api/createAcademicYear")]
        [HttpPost]
        public async Task<IActionResult> CreateAcademicYear([FromBody] AcademicYearDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _academicYearService.CreateAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/editAcademicYear")]
        [HttpPost]
        public async Task<IActionResult> EditAcademicYear([FromBody] AcademicYearDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _academicYearService.EditAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(400);
        }

        [Route("api/deleteAcademicYear")]
        [HttpPost]
        public async Task<IActionResult> DeleteAcademicYear(int id)
        {
            if(id != 0)
            {
                var result = await _academicYearService.DeleteAsync(id);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/academicYearManager")]
        [HttpGet]
        public async Task<IActionResult> GetAllAcademicYears(AcademicYearFilterDto filterDto)
        {
            var academicYearsViewModels = await _academicYearService.GetAllAcademicYears(filterDto);
            return Ok(academicYearsViewModels);
        }
        [Route("api/academicYear/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetYearAsync(int id)
        {
            return Ok(await _academicYearService.GetByIdAsync(id));
        }
    }
}
