﻿using ESDP.BL.Services;
using ESDP.Common.Dtos.TeachersDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator, Director, Headteacher")]
    public class TeacherController : Controller
    {
        private readonly TeachersService _teacherService;

        public TeacherController(TeachersService service)
        {
            _teacherService = service;
        }

        [Route("api/createTeacher")]
        [HttpPost]
        public async Task<IActionResult> CreateTeacher([FromBody] TeacherDto teacherDto)
        {
            if (ModelState.IsValid)
            {
                var result = await _teacherService.CreateAsync(teacherDto);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/editTeacher")]
        [HttpPost]
        public async Task<IActionResult> EditTeacher([FromBody] TeacherDto teacherDto)
        {
            if (ModelState.IsValid)
            {
                var result = await _teacherService.EditAsync(teacherDto);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/deleteTeacher")]
        [HttpPost]
        public async Task<IActionResult> DeleteTeacher(string id)
        {
            if(id != null)
            {
                var result = await _teacherService.DeleteAsync(id);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/teacherManager")]
        [HttpGet]
        public async Task<IActionResult> GetAllTeachers(TeacherFilterDto filterDto)
        {
            var teacherViewModels = await _teacherService.GetAllTeacher(filterDto);
            return Ok(teacherViewModels);
        }
    }
}
