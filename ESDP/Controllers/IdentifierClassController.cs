﻿using ESDP.BL.Services;
using ESDP.Common.Dtos.IdentifierClassDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator, Director, Headteacher")]
    public class IdentifierClassController : Controller
    {
        private readonly IdentifierClassService _identifierClassService;
        public IdentifierClassController(IdentifierClassService service)
        {
            _identifierClassService = service;
        }

        [Route("api/addIdentifierClass")]
        [HttpPost]
        public async Task<IActionResult> CreateIdentifierClass([FromBody] CreateIdentifierClassDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _identifierClassService.CreateAsync(model);
                if (result.Result)
                {
                    return Ok(result);
                }
                return StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/editIdentifierClass")]
        [HttpPost]
        public async Task<IActionResult> EditIdentifierClass([FromBody] CreateIdentifierClassDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _identifierClassService.EditAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/deleteIdentifierClass")]
        [HttpPost]
        public async Task<IActionResult> DeleteIdentifierClass(int id)
        {
            if (id != 0)
            {
                var result = await _identifierClassService.DeleteAsync(id);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/identifierClassManager")]
        [HttpGet]
        public async Task<IActionResult> GetAllIdentifierClasses(IdentifierClassFilterDto filterDto)
        {
            var academicSubjectViewModels = await _identifierClassService.GetAllIdentifierClass(filterDto);
            return Ok(academicSubjectViewModels);
        }

        [Route("api/identifierClass/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetIdentifierClass(int id)
        {
            return Ok(await _identifierClassService.FindByIdAsync(id));
        }
    }
}
