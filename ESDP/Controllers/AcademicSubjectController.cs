﻿using ESDP.BL.Services;
using ESDP.Common.Dtos.AcademicSubjectDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator, Director, Headteacher")]
    public class AcademicSubjectController : Controller
    {
        private readonly AcademicSubjectService _academicSubjectService;

        public AcademicSubjectController(AcademicSubjectService academicSubjectService)
        {
            _academicSubjectService = academicSubjectService;
        }

        [Route("api/addAcademicSubject")]
        [HttpPost]
        public async Task<IActionResult> CreateAcademicSubject([FromBody] AcademicSubjectDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _academicSubjectService.CreateAsync(model);
                if(result.Result)
                {
                    return Ok(result);
                }
                return StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/editAcademicSubject")]
        [HttpPost]
        public async Task<IActionResult> EditAcademicSubject([FromBody] AcademicSubjectDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _academicSubjectService.EditAsync(model);
                return result.Result == true  ? StatusCode(200, result) : StatusCode(500, result); 
            }
            return StatusCode(500);
        }

        [Route("api/deleteAcademicSubject")]
        [HttpPost]
        public async Task<IActionResult> DeleteAcademicSubject(int id)
        {
            if (id != 0)
            {
                var result = await _academicSubjectService.DeleteAsync(id);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(400);
        }

        [Route("api/academicSubject/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetSubjectAsync(int id)
        {
            return Ok(await _academicSubjectService.GetByIdAsync(id));
        }
        [Route("api/academicSubjectManager")]
        [HttpGet]
        public async Task<IActionResult> GetAllAcademicSubjects(AcademicSubjectFilterDto filterDto)
        {
            var academicSubjectViewModels = await _academicSubjectService.GetAllAcademicSubject(filterDto);
            return Ok(academicSubjectViewModels);
        }
    }
}
