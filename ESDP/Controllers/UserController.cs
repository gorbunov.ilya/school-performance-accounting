﻿using ESDP.BL;
using ESDP.Common.Dtos.UserDtos;
using ESDP.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserController : Controller
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }
        [Route("api/user/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetUserAsync(string id)
        {
            return Ok(await _userService.FindByIdEditModelAsync(id));
        }

        [Route("api/roles")]
        [HttpGet]
        public ActionResult GetRoles()
        {
            return Ok(_userService.GetAllRoles());
        }

        // /register
        [Route("api/add")]
        [HttpPost]
        public async Task<ActionResult> InsertUser([FromBody] RegisterDto model)
        {
            if (ModelState.IsValid)
            {
                var findUser = await _userService.FindByEmailAsync(model.Email);
                if(findUser == null)
                {
                    var result = await _userService.CreateAsync(model, model.Password);
                    if (result != null)
                    {
                        foreach (var role in model.Roles)
                        {
                            await _userService.AddToRoleAsync(result, role);
                        }
                        return Ok(new { Username = result.UserName });
                    }
                    return StatusCode(400);
                }
                return StatusCode(400);
            }
            return StatusCode(409);
        }

        [Route("api/usermanager")]
        [HttpGet]
        public async Task<IActionResult> GetAllUsers(UserFilterDto filter)
        {
            var userViewModels = await _userService.GetAllUsers(filter);
            return Ok(userViewModels);
        }

        [Route("api/delete")]
        [HttpPost]
        public async Task<ActionResult> DeleteUser(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var result = await _userService.DeleteUserAsync(id);
                if (result)
                {
                    return Ok();
                }
            }

            return BadRequest();
        }

        [Route("api/blockunblock")]
        [HttpPost]
        public async Task<ActionResult> BlockUnblockUser(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var result = await _userService.BlockUnblockUserAsync(id);
                if (result)
                {
                    return Ok();
                }
            }
            return BadRequest();
        }

        [Route("api/edit")]
        [HttpPost]
        public async Task<ActionResult> EditUser([FromBody] UserEditDto model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userService.FindByEmailAsync(model.Email);
                IList<string> roles = null;
                if(user != null)
                {
                   roles = await _userService.GetRolesAsync(user);
                }
                if(user == null || user.Email != model.Email ||
                    user.Name !=model.Name || user.Surname != model.Surname 
                    || user.MiddleName != model.MiddleName || roles != model.Roles)
                {
                    var result = await _userService.EditUser(model);
                    return result == true ? StatusCode(200) : StatusCode(400);
                }
                return StatusCode(400);
            }
            return StatusCode(409);
        }
    }
}
