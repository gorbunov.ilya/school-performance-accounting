﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using ESDP.BL;
using ESDP.Common.Dtos.UserDtos;
using ESDP.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ESDP.Controllers
{
    public class AuthController : Controller
    {
        private readonly UserService _userService;

        public AuthController(
            UserService userService)
        {
            _userService = userService;
        }

        [Route("login")] // /login
        [HttpPost]
        public async Task<ActionResult> Login([FromBody] LoginDto model)
        {
            
            if (ModelState.IsValid)
            {
                var user = await _userService.FindByEmailAsync(model.Email);
                if (user != null && await _userService.CheckPasswordAsync(user, model.Password))
                {
                    if (!user.IsBlocked)
                    {
                        
                        var token = await _userService.GetToken(user);
                        var roles =  await _userService.GetRolesAsync(user);

                        return Ok(new AuthorizeUserDto(){ Token = new JwtSecurityTokenHandler().WriteToken(token), Roles = roles });
                        
                    }
                    return StatusCode(403);
                }
                return Unauthorized();
            }
            return BadRequest("Invalid Login model");
        }
        
        
    }
}
