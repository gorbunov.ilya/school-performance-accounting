﻿using ESDP.BL.Services;
using ESDP.Common.Dtos.ReportPeriodDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESDP.Controllers
{
    [Authorize(Roles = "Administrator, Director, Headteacher")]
    public class ReportPeriodController : Controller
    {
        private readonly ReportPeriodService _reportPeriodService;
        public ReportPeriodController(ReportPeriodService service)
        {
            _reportPeriodService = service;
        }

        [Route("api/addReportPeriod")]
        [HttpPost]
        public async Task<IActionResult> CreateReportPeriod([FromBody] ReportPeriodDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _reportPeriodService.CreateAsync(model);
                if (result.Result)
                {
                    return Ok(result);
                }
                return StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/editReportPeriod")]
        [HttpPost]
        public async Task<IActionResult> EditReportPeriod([FromBody] ReportPeriodDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _reportPeriodService.EditAsync(model);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/deleteReportPeriod")]
        [HttpPost]
        public async Task<IActionResult> DeleteReportPeriod(int id)
        {
            if (id != 0)
            {
                var result = await _reportPeriodService.DeleteAsync(id);
                return result.Result ? StatusCode(200, result) : StatusCode(500, result);
            }
            return StatusCode(500);
        }

        [Route("api/reportPeriodManager")]
        [HttpGet]
        public async Task<IActionResult> GetAllAcademicYears(ReportPeriodFilterDto filterDto)
        {
            var academicSubjectViewModels = await _reportPeriodService.GetAllReportPeriods(filterDto);
            return Ok(academicSubjectViewModels);
        }
    }
}
