﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.AcademicYear
{
    public interface IAcademicYearRepository : IRepository<Entities.AcademicYear>
    {
        Task<Entities.AcademicYear> GetByIdIncludingAsync(int id);
        Task<IEnumerable<Entities.AcademicYear>> GetAllIncludingAsync();
    }
}
