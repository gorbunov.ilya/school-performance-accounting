﻿using ESDP.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.AcademicYear
{
    public class AcademicYearRepository : Repository<Entities.AcademicYear>, IAcademicYearRepository
    {
        public AcademicYearRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<Entities.AcademicYear> GetByIdIncludingAsync(int id)
        {
            return await _context.AcademicYears
                .Include(x => x.Periods)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Entities.AcademicYear>> GetAllIncludingAsync()
        {
            return await _context.AcademicYears
                .Include(x => x.Periods).ToListAsync();
        }
    }
}
