﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Repositories.IdentifierClass
{
    public interface IIdentifierClassRepository : IRepository<Entities.IdentifierClass>
    {

    }
}
