﻿using ESDP.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.TeachersAcademicSubjects
{
    public class TeachersAcademicSubjectsRepository : Repository<Entities.TeachersAcademicSubjects>, ITeachersAcademicSubjectsRepository
    {
        public TeachersAcademicSubjectsRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<Entities.TeachersAcademicSubjects> GetByIdIncludingAsync(int id)
        {
            return await _context.TeachersAcademicSubjects
                .Include(x => x.Subjects)
                    .ThenInclude(x => x.Periods)
                .Include(x => x.User)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Entities.TeachersAcademicSubjects>> GetAllIncludingAsync()
        {
            return await _context.TeachersAcademicSubjects
                .Include(x => x.Subjects)
                .Include(x => x.User).ToListAsync();
        }

        public async Task CreateRangeAsync(params Entities.TeachersAcademicSubjects[] teachersAcademicSubjects)
        {
            await _context.AddRangeAsync(teachersAcademicSubjects);
        }
        public void RemoveRange(params Entities.TeachersAcademicSubjects[] teachersAcademicSubjects)
        {
            _context.RemoveRange(teachersAcademicSubjects);
        }

        public void UpdateRange(params Entities.TeachersAcademicSubjects[] teachersAcademicSubjects)
        {
            _context.UpdateRange(teachersAcademicSubjects);
        }
    }
}
