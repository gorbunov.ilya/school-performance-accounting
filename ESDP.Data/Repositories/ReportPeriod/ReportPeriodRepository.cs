﻿using ESDP.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.ReportPeriod
{
    public class ReportPeriodRepository : Repository<Entities.ReportPeriod>, IReportPeriodRepository
    {
        public ReportPeriodRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<Entities.ReportPeriod> GetByIdIncludingAsync(int id)
        {
            return await _context.ReportPeriods
                .Include(x => x.Classess)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Entities.ReportPeriod>> GetAllIncludingAsync()
        {
            return await _context.ReportPeriods
                .Include(x => x.Classess).ToListAsync();
        }
    }
}
