﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.ReportPeriod
{
    public interface IReportPeriodRepository : IRepository<Entities.ReportPeriod>
    {
        Task<Entities.ReportPeriod> GetByIdIncludingAsync(int id);
        Task<IEnumerable<Entities.ReportPeriod>> GetAllIncludingAsync();
    }
}
