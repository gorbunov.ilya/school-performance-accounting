﻿using ESDP.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories.AcademicSubject
{
    public class AcademicSubjectRepository : Repository<Entities.AcademicSubject>, IAcademicSubjectRepository
    {
        public AcademicSubjectRepository(ApplicationDbContext context) : base(context)
        {

        }

        public async Task<Entities.AcademicSubject> GetByIdIncludingAsync(int id)
        {
            return await _context.AcademicSubjects
                .Include(x => x.Periods)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Entities.AcademicSubject>> GetAllIncludingAsync()
        {
            return await _context.AcademicSubjects
                .Include(x => x.Teachers)
                .Include(x => x.Periods).ToListAsync();
        }
    }
}
