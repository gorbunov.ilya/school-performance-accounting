﻿using ESDP.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        Task CreateAsync(T entity);
        Task<List<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        void UpdateAsync(T entity);
        void RemoveAsync(T entity);
    }
}
