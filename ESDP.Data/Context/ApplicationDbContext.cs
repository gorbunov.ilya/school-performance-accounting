﻿using System;
using System.Collections.Generic;
using System.Text;
using ESDP.Data.Entities;
using ESDP.Data.EntityConfigurations.BusinessEntityConfigurations;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using User = ESDP.Data.Entities.User;

namespace ESDP.Data.Context
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }
        public DbSet<AcademicSubject> AcademicSubjects { get; set; }
        public DbSet<TeachersAcademicSubjects> TeachersAcademicSubjects { get; set; }
        public DbSet<IdentifierClass> IdentifierClasses { get; set; }
        public DbSet<ReportPeriod> ReportPeriods { get; set; }
        public DbSet<AcademicYear> AcademicYears { get; set; }
        public DbSet<AcademicSubjectsPeriods> AcademicSubjectsPeriods { get; set; }
        public DbSet<AcademicClass> Classes { get; set; }
        public DbSet<Grades> Grades { get; set; }
        public DbSet<AcademicClassessReportPeriods> AcademicClassessReportPeriods { get; set; }
        public DbSet<AcademicYearsReportPeriods> AcademicYearsReportPeriods { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new AcademicSubjectConfiguration());
            builder.ApplyConfiguration(new TeachersAcademicSubjectsConfiguration());
            builder.ApplyConfiguration(new IdentifierClassConfiguration());
            builder.ApplyConfiguration(new AcademicYearConfiguration());
            builder.ApplyConfiguration(new ReportPeriodConfiguration());
            builder.ApplyConfiguration(new AcademicSubjectsPeriodsConfiguration());
            builder.ApplyConfiguration(new AcademicClassConfiguration());
            builder.ApplyConfiguration(new GradesConfiguration());
            builder.ApplyConfiguration(new AcademicClassessReportPeriodsConfiguration());
            builder.ApplyConfiguration(new AcademicYearsReportPeriodsConfiguration());
        }
    }

}