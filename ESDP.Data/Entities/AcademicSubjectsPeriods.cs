﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class AcademicSubjectsPeriods : Entity
    {
        public int? AcademicSubjectsId { get; set; }
        public AcademicSubject Subject { get; set; }

        public int? ReportPeriodId { get; set; }
        public ReportPeriod Period { get; set; }
    }
}
