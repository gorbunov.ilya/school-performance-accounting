﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class AcademicClass : Entity
    {
        public int IdentifierClassId { get; set; }
        public IdentifierClass Identifier { get; set; }

        public int? AcademicYearId { get; set; }
        public AcademicYear Year { get; set; }

        public ICollection<Grades> Grades { get; set; }
        public ICollection<AcademicClassessReportPeriods> ReportPeriods { get; set; }
    }
}
