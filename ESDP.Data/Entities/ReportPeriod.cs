﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class ReportPeriod : Entity
    {
        public string Name { get; set; }

        public ICollection<AcademicYearsReportPeriods> Years { get; set; }
        public ICollection<AcademicSubjectsPeriods> Subjects { get; set; }
        public ICollection<AcademicClassessReportPeriods> Classess { get; set; }
    }
}
