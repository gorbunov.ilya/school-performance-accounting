﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class AcademicClassessReportPeriods : Entity
    {
        public int? AcademicClassId { get; set; }
        public AcademicClass Class { get; set; }

        public int? ReportPeriodId { get; set; }
        public ReportPeriod ReportPeriod { get; set; }
    }
}
