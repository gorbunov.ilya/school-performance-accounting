﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class AcademicYearsReportPeriods : Entity
    {
        public int? AcademicYearId { get; set; }
        public AcademicYear Year { get; set; }

        public int? ReportPeriodId { get; set; }
        public ReportPeriod Period { get; set; }
    }
}
