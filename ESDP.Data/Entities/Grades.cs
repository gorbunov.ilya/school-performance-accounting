﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class Grades : Entity
    {
        public int FiveCount { get; set; }
        public int FourCount { get; set; }
        public int ThreeCount { get; set; }
        public int TwoCount { get; set; }
        public int OneCount { get; set; }

        public int? AcademicClassId { get; set; }
        public AcademicClass Class { get; set; }

        public int? AcademicSubjectsId { get; set; }
        public AcademicSubject AcademicSubjects { get; set; }
    }
}
