﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class IdentifierClass : Entity
    {
        public char Letter { get; set; }
        public int Parallel { get; set; }

        public int? AcademicClassId { get; set; }
        public AcademicClass Class { get; set; }
    }
}
