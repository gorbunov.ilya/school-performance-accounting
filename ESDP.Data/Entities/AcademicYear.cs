﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.Entities
{
    public class AcademicYear : Entity
    {
        public string Name { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsCurrent { get; set; }

        public ICollection<AcademicYearsReportPeriods> Periods { get; set; }
        public ICollection<AcademicClass> Classes { get; set; }
    }
}
