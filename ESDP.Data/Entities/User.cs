﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ESDP.Data.Entities
{
    public class User : IdentityUser
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        public string MiddleName { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsDeleted { get; set; }

        public ICollection<TeachersAcademicSubjects> Subjects { get; set; }
    }
}