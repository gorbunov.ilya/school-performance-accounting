﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class AcademicSubjectsPeriodsConfiguration : IEntityTypeConfiguration<AcademicSubjectsPeriods>
    {
        public void Configure(EntityTypeBuilder<AcademicSubjectsPeriods> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Subject).WithMany(x => x.Periods).HasForeignKey(x => x.AcademicSubjectsId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Period).WithMany(x => x.Subjects).HasForeignKey(x => x.ReportPeriodId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
