﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class GradesConfiguration : IEntityTypeConfiguration<Grades>
    {
        public void Configure(EntityTypeBuilder<Grades> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.AcademicSubjects).WithMany(x => x.Grades).HasForeignKey(x => x.AcademicSubjectsId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Class).WithMany(x => x.Grades).HasForeignKey(x => x.AcademicClassId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
