﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class AcademicClassessReportPeriodsConfiguration : IEntityTypeConfiguration<AcademicClassessReportPeriods>
    {
        public void Configure(EntityTypeBuilder<AcademicClassessReportPeriods> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.ReportPeriod).WithMany(x => x.Classess).HasForeignKey(x => x.ReportPeriodId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Class).WithMany(x => x.ReportPeriods).HasForeignKey(x => x.AcademicClassId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
