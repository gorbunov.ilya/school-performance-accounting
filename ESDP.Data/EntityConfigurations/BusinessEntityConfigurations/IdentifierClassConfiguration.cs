﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class IdentifierClassConfiguration : IEntityTypeConfiguration<IdentifierClass>
    {
        public void Configure(EntityTypeBuilder<IdentifierClass> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Class).WithOne(x => x.Identifier).HasForeignKey<AcademicClass>(x => x.IdentifierClassId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
