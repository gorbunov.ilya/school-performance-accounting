﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class AcademicClassConfiguration : IEntityTypeConfiguration<AcademicClass>
    {
        public void Configure(EntityTypeBuilder<AcademicClass> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Identifier).WithOne(x => x.Class).HasForeignKey<IdentifierClass>(x => x.AcademicClassId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Year).WithMany(x => x.Classes).HasForeignKey(x => x.AcademicYearId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
