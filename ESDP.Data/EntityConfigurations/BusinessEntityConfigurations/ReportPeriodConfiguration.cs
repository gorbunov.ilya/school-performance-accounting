﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class ReportPeriodConfiguration : IEntityTypeConfiguration<ReportPeriod>
    {
        public void Configure(EntityTypeBuilder<ReportPeriod> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}
