﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class AcademicYearsReportPeriodsConfiguration : IEntityTypeConfiguration<AcademicYearsReportPeriods>
    {
        public void Configure(EntityTypeBuilder<AcademicYearsReportPeriods> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Period).WithMany(x => x.Years).HasForeignKey(x => x.ReportPeriodId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Year).WithMany(x => x.Periods).HasForeignKey(x => x.AcademicYearId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
