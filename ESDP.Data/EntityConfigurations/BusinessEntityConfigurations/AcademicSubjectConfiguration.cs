﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class AcademicSubjectConfiguration : IEntityTypeConfiguration<AcademicSubject>
    {
        public void Configure(EntityTypeBuilder<AcademicSubject> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasColumnType("NVARCHAR(50)").HasMaxLength(50).IsRequired(true);
        }
    }
}
