﻿using ESDP.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESDP.Data.EntityConfigurations.BusinessEntityConfigurations
{
    public class AcademicYearConfiguration : IEntityTypeConfiguration<AcademicYear>
    {
        public void Configure(EntityTypeBuilder<AcademicYear> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasColumnType("NVARCHAR(100)").HasMaxLength(100).IsRequired(true);
            builder.Property(x => x.BeginDate).IsRequired(false);
            builder.Property(x => x.EndDate).IsRequired(false);
            builder.Property(x => x.IsCurrent).IsRequired(true);

            builder.HasMany(x => x.Classes).WithOne(x => x.Year).HasForeignKey(x => x.AcademicYearId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
