﻿using ESDP.Data.Context;
using ESDP.Data.Entities;
using ESDP.Data.Repositories;
using ESDP.Data.Repositories.AcademicSubject;
using ESDP.Data.Repositories.AcademicYear;
using ESDP.Data.Repositories.IdentifierClass;
using ESDP.Data.Repositories.ReportPeriod;
using ESDP.Data.Repositories.TeachersAcademicSubjects;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly ConcurrentDictionary<Type, object> _repositories;

        public IAcademicSubjectRepository AcademicSubjectRepository { get; }
        public IIdentifierClassRepository IdentifierClassRepository { get; }
        public IReportPeriodRepository ReportPeriodRepository { get; }
        public IAcademicYearRepository AcademicYearRepository { get; }
        public ITeachersAcademicSubjectsRepository TeachersAcademicSubjectsRepository { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            _repositories = new ConcurrentDictionary<Type, object>();

            AcademicSubjectRepository = new AcademicSubjectRepository(context);
            IdentifierClassRepository = new IdentifierClassRepository(context);
            ReportPeriodRepository = new ReportPeriodRepository(context);
            AcademicYearRepository = new AcademicYearRepository(context);
            TeachersAcademicSubjectsRepository = new TeachersAcademicSubjectsRepository(context);
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity
        {
            return _repositories.GetOrAdd(typeof(TEntity), new Repository<TEntity>(_context)) as IRepository<TEntity>;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}
