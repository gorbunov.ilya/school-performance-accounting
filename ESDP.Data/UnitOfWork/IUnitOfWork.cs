﻿using ESDP.Data.Entities;
using ESDP.Data.Repositories;
using ESDP.Data.Repositories.AcademicSubject;
using ESDP.Data.Repositories.AcademicYear;
using ESDP.Data.Repositories.IdentifierClass;
using ESDP.Data.Repositories.ReportPeriod;
using ESDP.Data.Repositories.TeachersAcademicSubjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ESDP.Data.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IAcademicSubjectRepository AcademicSubjectRepository { get; }
        IIdentifierClassRepository IdentifierClassRepository { get; }
        IReportPeriodRepository ReportPeriodRepository { get; }
        IAcademicYearRepository AcademicYearRepository { get; }
        ITeachersAcademicSubjectsRepository TeachersAcademicSubjectsRepository { get; }

        Task<int> CompleteAsync();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;

    }
}
